package com.binnacle;

import com.opencsv.CSVReader;
import io.vavr.Tuple;
import me.tongfei.progressbar.ProgressBar;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcSun15HttpTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;
import org.infinispan.multimap.api.embedded.MultimapCache;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;

public class HelperFunctions {
    public static final String URL_FORMAT = "https://%s.dev.odoo.com";
    public static final String PASSWORD = "binnacle";
    public static final String USER = "admin";
    public static final String SERVER = "binnacle-data-import-api-834045";
    public static HashMap<String, Integer> measureCache = new HashMap<>();
    public static HashMap<String, Integer> stateLookup = new HashMap<>();
    public static HashMap<String, Integer> countryToId = new HashMap<>();
    public static HashMap<String, Integer> stateToFiscalPosition = new HashMap<>();
    private static Map<String, Integer> productCodeLookup = new HashMap<>();
    public static Map<Pair<String, Integer>, Integer> stateLook = new HashMap<>();
    public static Map<String, Object> measures = setupUnitOfMeasures();
    static {
        measures = setupUnitOfMeasures();
        try {
            int uid = HelperFunctions.authenticateToOdoo();
            stateLook = getStateLookup(uid);
            countryToId = getCountryLookup(uid);
            stateToFiscalPosition = createFiscalLookup(uid);
        } catch (MalformedURLException | XmlRpcException e) {
            e.printStackTrace();
        }
    }

    public static XmlRpcClient getModelConfig() throws MalformedURLException {

        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    @Override
                    public void checkClientTrusted(X509Certificate[] arg0, String arg1)
                            throws CertificateException {}

                    @Override
                    public void checkServerTrusted(X509Certificate[] arg0, String arg1)
                            throws CertificateException {}
                }
        };

        SSLContext sc=null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }


        String url = String.format(URL_FORMAT, HelperFunctions.SERVER);
        final XmlRpcClient models = new XmlRpcClient() {{
            setConfig(new XmlRpcClientConfigImpl() {{
                setServerURL(new URL(String.format("%s/xmlrpc/2/object", url)));
            }});
        }};

        XmlRpcSun15HttpTransportFactory transportFactory =
                new XmlRpcSun15HttpTransportFactory(models);
        transportFactory.setSSLSocketFactory(sc.getSocketFactory());
        transportFactory.setProxy(new Proxy(Proxy.Type.HTTP,new InetSocketAddress("localhost",8080)));
        models.setTransportFactory(transportFactory);


        return models;
    }

    public static void createShippingProduct(int uid) throws MalformedURLException, XmlRpcException {
        List<Object> result = asList((Object[]) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                "product.template", "search_read",
                asList(asList(
                        asList("default_code", "=", "bvshipping"))),
                new HashMap() {{
                    put("fields", asList("id"));
                    put("limit", 1);
                }}
        )));

        if(result == null || result.isEmpty()){
            HashMap<String,Object> custMap = new HashMap<>();
            custMap.put("name","Shipping");
            custMap.put("type","service");
            custMap.put("sale_ok",false);
            custMap.put("purchase_ok",false);
            custMap.put("default_code","bvshipping");
            custMap.put("list_price",0);

            HelperFunctions.getModelConfig().execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "product.product", "create",
                    Collections.singletonList(custMap)
            ));
        }
    }


    public static void appendUsingBufferedWriter(String filePath, String text, int noOfLines) {
        File file = new File(filePath);
        FileWriter fr = null;
        BufferedWriter br = null;
        try {
            // to append to file, you need to initialize FileWriter using below constructor
            fr = new FileWriter(file, false);
            br = new BufferedWriter(fr);
            for (int i = 0; i < noOfLines; i++) {
                // you can use write or append method
                br.write(text);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Map<String, Object> setupUnitOfMeasures() {
        Map<String, Object> lookup = new HashMap<>() {{
            put("YRD", Tuple.of("length", 0.9144));
            put("YD", Tuple.of("length", 0.9144));
            put("SET", "units");
            put("SIN", "units");
            put("QT", "qt");
            put("QT.", "qt");
            put("SIN.", "qt");
            put("PT", "units");
            put("PT.", "units");
            put("PR", "units");
            put("PR.", "units");
            put("SEC", "units");
            put("PKG", "units");
            put("PAD", "units");
            put("PA", "units");
            put("MTR", "m");
            put("MT", "m");
            put("IN", "inches");
            put("GA", "gals");
            put("GAL", "gals");
            put("FT", "foot(ft)");
            put("FT.", "foot(ft)");
            put("EA", "units");
            put("EA.", "units");
            put("EACH.", "units");
            put("EACH", "units");
            put("LB", "lbs");
            put("64", Tuple.of("unit", 64.0));
            put("4PK", Tuple.of("unit", 4.0));
            put("10PK", Tuple.of("unit", 10.0));
            put("1LT", "liters");
            put("30 FEET", Tuple.of("length", 9.144));
            put("PAIR", Tuple.of("unit", 2.0));
        }};
        return lookup;
    }

    public static void loadTableIntoOffheap(String id, String query, MultimapCache<String, Map<String,Object>> offheap ){
        try (Connection connection = ImportMain.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setFetchSize(10000);
            ResultSet rs = preparedStatement.executeQuery();
            HelperFunctions.resultSetToMap(rs,id, offheap);
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void resultSetToMap(ResultSet rs, String id, MultimapCache<String, Map<String,Object>> offheap) throws SQLException {

        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i), rs.getObject(i));
            }
            offheap.put((String)row.get(id),row);
        }
    }


    public static void loadTableIntoRedis(int db, String id, String query) {

        Config config = new Config();
        config.useSingleServer()
                .setDatabase(db)
                .setPassword("k0clOVxJjYc6Om2BBazwUuyaY6AAdKKrl9FEGS9StkE=")
                .setAddress("redis://bin-redis.redis.cache.windows.net:6379");
        RedissonClient client = Redisson.create(config);

        try (Connection connection = ImportMain.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setFetchSize(10000);
            ResultSet rs = preparedStatement.executeQuery();
            HelperFunctions.resultSetToList(rs, client, id);
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }





    public static void resultSetToList(ResultSet rs, RedissonClient client, String id) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<String, Object>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i), rs.getObject(i));
            }
            RBucket<List<Map<String, Object>>> bucket = client.getBucket((String) row.get(id));
            List<Map<String, Object>> add = bucket.get();
            if (add == null)
                add = new ArrayList<>();
            add.add(row);
            bucket.set(add);
            add.clear();
            row.clear();
        }

    }

    public static Map<Pair<String,Integer>, Integer> getStateLookup(int uid) throws MalformedURLException, XmlRpcException {
        HashMap<Pair<String,Integer>,Integer> unitsOfMeasure = new HashMap<>();
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "res.country.state", "search_read",
                        Collections.emptyList(),
                        new HashMap<>() {{
                            put("fields", asList("id","country_id","code"));
                        }})));

        if (ids.isEmpty())
            return new HashMap<>();
        for(Object o : ids){
            HashMap map = (HashMap)o;
            Object[] cId = (Object[]) map.get("country_id");
            unitsOfMeasure.put(Pair.of(((String)map.get("code")).toLowerCase(),(Integer)cId[0]),(Integer)map.get("id"));
        }
        return unitsOfMeasure;
    }

    public static HashMap<String,Integer> getCountryLookup(int uid) throws MalformedURLException, XmlRpcException {
        HashMap<String,Integer> unitsOfMeasure = new HashMap<>();
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "res.country", "search_read",
                        Collections.emptyList(),
                        new HashMap<>() {{
                            put("fields", asList("id","code"));
                        }})));

        if (ids.isEmpty())
            return new HashMap<>();
        for(Object o : ids){
            HashMap map = (HashMap)o;
            unitsOfMeasure.put(((String)map.get("code")).toLowerCase(),(Integer)map.get("id"));
        }
        return unitsOfMeasure;
    }



    public static void deleteAll(int uid, String table) throws XmlRpcException, MalformedURLException {
        XmlRpcClient models = HelperFunctions.getModelConfig();
        List ids = Arrays.asList((Object[]) models.execute("execute_kw", Arrays.asList(
                HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                table, "search",
                Arrays.asList(Collections.emptyList()
                ))));

        try {
            models.execute("execute_kw", Arrays.asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    table, "unlink",
                    Arrays.asList(ids)));
        } catch (XmlRpcException e) {
            System.out.println("Tried Deleting Everythong, Failed");
        }
        ProgressBar deleteProgress = new ProgressBar("Deleting " + table, ids.size());
        for (Object id : ids) {
            try {
                models.execute("execute_kw", Arrays.asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        table, "unlink",
                        Arrays.asList(Arrays.asList(id))));
                deleteProgress.step();
            } catch (XmlRpcException e) {
                System.out.println("Error Deleting " + table + " , Probably Nothing To Worry About");
            }
        }
        deleteProgress.close();
    }

    public static String saveUnitOfMeasures() throws MalformedURLException, XmlRpcException {
        StringBuilder builder = new StringBuilder();
        builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<odoo noupdate=\"0\">\n" +
                " <data>");
        Map<String, Object> lookup = setupUnitOfMeasures();
        for (Map.Entry<String, Object> e : lookup.entrySet()) {
            if (e.getValue() instanceof Tuple) {
                Tuple aRR = (Tuple) e.getValue();
                Double factor = (Double) aRR.toSeq().get(1);
                String category = (String)aRR.toSeq().get(0);
                String xml = String.format("\n<record id=\"bin-uom-%s\" model=\"uom.uom\">\n" +
                        "<field name=\"name\">%s</field>\n"+
                        "<field name=\"uom_type\">%s</field>\n" +
                        "<field name=\"factor\">%s</field>\n" +
                        "<field name=\"category_id\" ref=\"%s\"/>\n" +
                        "</record>\n",e.getKey().replaceAll("\\s","-").toLowerCase(),e.getKey(),factor > 1 ? "bigger" : "smaller",factor,category);
                builder.append(xml);
            }

        }
        builder.append("</data>\n</odoo>");
        return builder.toString();
    }

    public static HashMap<String,Object> getAllUnitsOfMeasure(int uid) throws MalformedURLException, XmlRpcException {
        HashMap<String,Object> unitsOfMeasure = new HashMap<>();
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "uom.uom", "search_read",
                        Collections.emptyList(),
                        new HashMap<>() {{
                            put("fields", asList("id","name","category_id"));
                        }})));

        if (ids.isEmpty())
            return new HashMap<>();
        for(Object o : ids){
            HashMap map = (HashMap)o;
            unitsOfMeasure.put(((String)map.get("name")).toLowerCase(),map);
        }
        return unitsOfMeasure;
    }

    public static HashMap<String,Object> getPaymentTerms(int uid) throws MalformedURLException, XmlRpcException {
        HashMap<String,Object> unitsOfMeasure = new HashMap<>();
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "uom.uom", "search_read",
                        Collections.emptyList(),
                        new HashMap<>() {{
                            put("fields", asList("id","name","category_id"));
                        }})));

        if (ids.isEmpty())
            return new HashMap<>();
        for(Object o : ids){
            HashMap map = (HashMap)o;
            unitsOfMeasure.put(((String)map.get("name")).toLowerCase(),map);
        }
        return unitsOfMeasure;
    }

    public static HashMap<String,Object> getUnitOfMeasureCategories(int uid) throws MalformedURLException, XmlRpcException {
        HashMap<String,Object> unitsOfMeasure = new HashMap<>();
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "uom.category", "search_read",
                        Collections.emptyList(),
                        new HashMap<>() {{
                            put("fields", asList("id","name","measure_type"));
                        }})));

        if (ids.isEmpty())
            return new HashMap<>();
        for(Object o : ids){
            HashMap map = (HashMap)o;
            unitsOfMeasure.put(((String)map.get("measure_type")).toLowerCase(),map);
        }
        return unitsOfMeasure;
    }

    public static int getUnitOfMeasureId(String k, int uid) throws MalformedURLException, XmlRpcException {
        if (measureCache.containsKey(k))
            return measureCache.get(k);
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "uom.uom", "search",
                        Collections.singletonList(Collections.singletonList(
                                asList("name", "=", k)
                        )),
                        new HashMap<>() {{
                            put("limit", 1);
                        }})));
        if (ids.isEmpty())
            return -1;

        int g = (int) ids.get(0);
        measureCache.put(k, g);
        return g;
    }

    public static int howManyIn(String query) throws SQLException {
        try (Connection connection = ImportMain.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query);) {
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }

    public static int getUnitOfMeasureId() {
        return 0;
    }

    public static String getValueFromResultSet(String columnName, ResultSet set) throws SQLException {
        String val = set.getString(columnName);
        if (val != null)
            return val.strip();
        return null;
    }

    public static int authenticateToOdoo() throws XmlRpcException, MalformedURLException {
        String url = String.format(URL_FORMAT, HelperFunctions.SERVER);
        final XmlRpcClient client = new XmlRpcClient();
        final XmlRpcClientConfigImpl common_config = new XmlRpcClientConfigImpl();
        common_config.setServerURL(new URL(String.format("%s/xmlrpc/2/common", url)));
        int uid = (int) client.execute(common_config, "authenticate", asList(HelperFunctions.SERVER, HelperFunctions.USER, HelperFunctions.PASSWORD, emptyMap()));
        return uid;
    }

    public static Integer findStateIdByCountryIdAndName(int uid, String code, Integer countryId) throws SQLException, MalformedURLException, XmlRpcException {
        String key = String.format("%s:%s", countryId, code);
        if (stateLookup.containsKey(key))
            return stateLookup.get(key);
        try {
            XmlRpcClient models = HelperFunctions.getModelConfig();
            List<Object> result = asList((Object[]) models.execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "res.country.state", "search_read",
                    asList(asList(
                            asList("code", "=", code),
                            asList("country_id", "=", countryId))),
                    new HashMap() {{
                        put("fields", asList("id"));
                        put("limit", 1);
                    }}
            )));

            if (result == null || result.isEmpty()) {
                stateLookup.put(key, -1);
                return -1;
            }

            Map<String, Integer> resultInt = (Map<String, Integer>) result.get(0);
            stateLookup.put(key, resultInt.get("id"));
            return resultInt.get("id");
        } catch (MalformedURLException | XmlRpcException e) {
            System.out.println("Error");
            return -1;
        }
    }

    public static Integer getOdooPaymentTerms(String bvTerms){
        Integer termCode = -1;
        switch(bvTerms){
            case "01":
                termCode = 4;
                break;
            case "BOOKING":
                termCode = 9;
                break;
            case "08":
                termCode = 27;
                break;
            case "05":
                termCode = 28;
                break;
            case "04":
                termCode = 29;
                break;
            case "06":
                termCode = 33;
                break;
            case "07":
                termCode = 34;
                break;
            default:
                break;
        }
        return termCode;
    }


    public static Integer findCountryIdByName(int uid, String code) throws SQLException, MalformedURLException, XmlRpcException {
        if (countryToId.containsKey(code))
            return countryToId.get(code);
        try {
            Connection connection = ImportMain.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT NAME FROM COUNTRY WHERE CODE = ?");
            preparedStatement.setString(1, code);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                String name = rs.getString("NAME");
                if (name != null) {
                    name = name.strip();
                    XmlRpcClient models = HelperFunctions.getModelConfig();
                    List<Object> result = asList((Object[]) models.execute("execute_kw", asList(
                            HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                            "res.country", "search_read",
                            asList(asList(
                                    asList("name", "=", name))),
                            new HashMap() {{
                                put("fields", asList("id"));
                                put("limit", 1);
                            }}
                    )));

                    Map<String, Integer> resultInt = (Map<String, Integer>) result.get(0);
                    countryToId.put(code, resultInt.get("id"));
                    return resultInt.get("id");
                }
            }
        } catch (SQLException | MalformedURLException | XmlRpcException e) {
            System.out.println("Error");
            return -1;
        }
        countryToId.put(code, -1);
        return -1;
    }

    public static Integer findProductByCode(String productCode, int uid) throws MalformedURLException {
        try {
            XmlRpcClient models = HelperFunctions.getModelConfig();
            List<Object> result = asList((Object[]) models.execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "product.template", "search_read",
                    asList(asList(
                            asList("default_code", "=", productCode))),
                    new HashMap() {{
                        put("fields", asList("id"));
                        put("limit", 1);
                    }}
            )));

            HashMap<String, Integer> resultInt = (HashMap<String, Integer>) result.get(0);

            List<Object> productResult = asList((Object[]) models.execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "product.product", "search_read",
                    asList(asList(
                            asList("product_tmpl_id", "=", resultInt.get("id")))),
                    new HashMap() {{
                        put("fields", asList("id"));
                        put("limit", 1);
                    }}
            )));

            resultInt = (HashMap<String, Integer>) productResult.get(0);
            productCodeLookup.put(productCode, resultInt.get("id"));
            return resultInt.get("id");
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        productCodeLookup.put(productCode, -1);
        return -1;
    }

    public static String getUnitOfMeasure(String unitOfMeasure) {
        if(!measures.containsKey(unitOfMeasure.strip())){
            System.out.println("NOT FOUND " + unitOfMeasure);
            return (String)measures.get("PT");
        } else{
            Object mes = measures.get(unitOfMeasure.strip());
            if (mes instanceof Tuple) {
                return "binnacle.bin-uom-" + unitOfMeasure.strip().toLowerCase().replaceAll("\\s","-");
            } else {
                return (String)mes;
            }
        }
    }


    public static void createPosAndUnknownCustomer() throws XmlRpcException, MalformedURLException {
        int uid = HelperFunctions.authenticateToOdoo();
        Customer pos = new Customer();
        pos.setName("Binnacle POS Customer");
        CustomerAddress posAddress = new CustomerAddress();
        posAddress.setCity("Halifax");
        posAddress.setCountry("CDN");
        posAddress.setState("NS");
        posAddress.setStreet("1065 Purcells Cove Rd");
        posAddress.setZip("B3N1R2");
        posAddress.setType("B");
        posAddress.setTel("");
        posAddress.setStreet2("");
        posAddress.setEmail("");
        pos.getAddressList().add(posAddress);
        pos.setId("pos");

        Customer unknown = new Customer();
        unknown.setName("Binnacle Unknown Customer");
        CustomerAddress unknownAddress = new CustomerAddress();
        unknownAddress.setCity("Halifax");
        unknownAddress.setCountry("CDN");
        unknownAddress.setState("NS");
        unknownAddress.setStreet("1065 Purcells Cove Rd");
        unknownAddress.setZip("B3N1R2");
        unknownAddress.setType("B");
        unknownAddress.setTel("");
        unknownAddress.setStreet2("");
        unknownAddress.setEmail("");
        unknown.getAddressList().add(posAddress);
        unknown.setId("unknown");


        try {

            CustomerAddress address = pos.getMap().peek();
            HashMap<String, Object> contact = address.getMap();
            contact.put("name", pos.getName());
            contact.put("company_type", "person");
            contact.put("ref", pos.getId());
            contact.put("is_company", true);
            contact.put("type", "contact");
            contact.put("customer_rank", "1");
            int id = (int) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "res.partner", "create",
                    Collections.singletonList(contact)
            ));
            posAddress.setOdooId(id);
        } catch (XmlRpcException | MalformedURLException e) {
            System.out.println("Failed to add POS Customer");
        }

        try {

            CustomerAddress address = unknown.getMap().peek();
            HashMap<String, Object> contact = address.getMap();
            contact.put("name", unknown.getName());
            contact.put("company_type", "person");
            contact.put("ref", unknown.getId());
            contact.put("is_company", true);
            contact.put("type", "contact");
            contact.put("customer_rank", "1");
            contact.put("supplier_rank", "1");
            int id = (int) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "res.partner", "create",
                    Collections.singletonList(contact)
            ));
            unknownAddress.setOdooId(id);
        } catch (XmlRpcException | MalformedURLException e) {
            System.out.println("Failed to add POS Customer");
        }

    }



    public static Map<String, Pair<Integer,String>>  loadProductsFromOdoo() throws MalformedURLException, XmlRpcException {
        Map<String,Pair<Integer,String>> products = new HashMap<>();
        XmlRpcClient models = HelperFunctions.getModelConfig();
        List<Object> result = asList((Object[]) models.execute("execute_kw", asList(
                HelperFunctions.SERVER, HelperFunctions.authenticateToOdoo(), HelperFunctions.PASSWORD,
                "product.template", "search_read",
                asList(asList(
                        asList("id", ">", 29))),
                new HashMap() {{
                    put("fields", asList("id","default_code","uom_po_id"));
                }}
        )));

        List<Object> result2 = asList((Object[]) models.execute("execute_kw", asList(
                HelperFunctions.SERVER, HelperFunctions.authenticateToOdoo(), HelperFunctions.PASSWORD,
                "product.product", "search_read",
                Collections.emptyList(),
                new HashMap() {{
                    put("fields", asList("id","product_tmpl_id"));
                }}
        )));

        HashMap<Integer,Integer> productTemplLook = new HashMap<>();
        for(Object o : result2){
            HashMap map = (HashMap)o;
            Object[] ptml = (Object[]) map.get("product_tmpl_id");
            productTemplLook.put((Integer)ptml[0],(Integer)map.get("id"));

        }

        for(Object o : result){
            HashMap map = (HashMap)o;
            Object[] poArr = (Object[])map.get("uom_po_id");
            if(poArr[1] instanceof String && map.get("default_code") instanceof String)
                products.put((String)map.get("default_code"),Pair.of(productTemplLook.get(map.get("id")),((String)poArr[1]).toLowerCase()));
            else{
                Integer a = poArr.length;
            }

        }
        return products;
    }


    public static int createAccount(String orderId, String customerId, Integer partnerId, int uid) {
        int a = -1;
        try {
            Map<String, Object> contact = new HashMap<>();
            contact.put("name",orderId);
            contact.put("code",customerId);
            contact.put("company_id",1);
            contact.put("partner_id",partnerId);
            a = (int) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "account.analytic.account", "create",
                    Collections.singletonList(contact)
            ));
        } catch (XmlRpcException | MalformedURLException e) {

        }

        return a;
    }

    public static String getStateFromZipCode(int searchColumnIndex, String searchString) throws IOException {
        String resultRow = null;
        BufferedReader br = new BufferedReader(new FileReader("geo-data.csv"));
        String line;
        while ( (line = br.readLine()) != null ) {
            String[] values = line.split(",");
            if(values[searchColumnIndex].equalsIgnoreCase(searchString)) {
                resultRow = values[2];
                break;
            }
        }
        br.close();
        return resultRow;
    }

    public static Pair<Set<String>,Set<String>> loadExistingCustomer(int uid) throws XmlRpcException, MalformedURLException {
        Set<String>  hm = new HashSet<String>();
        Set<String>  hm2 = new HashSet<String>();
        XmlRpcClient models = HelperFunctions.getModelConfig();
        List<Object> result = asList((Object[]) models.execute("execute_kw", asList(
                HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                "res.partner", "search_read",
               /* asList(asList(
                        asList("id", ">", 280)
                )), */
                Collections.emptyList(),
                new HashMap() {{
                    put("fields", asList(
                            "ref",
                            "supplier_rank",
                            "customer_rank",
                            "parent_id"
                    ));
                }}
        )));

        for(Object o : result){
            HashMap map = (HashMap)o;
            Object ot = map.get("ref");

            if(!(ot instanceof String)){
                System.out.println("Ref no string " + map.get("id"));
                continue;
            }
            Integer cRank = (Integer)map.get("customer_rank");
            Integer parentId = -1;
            if(!(map.get("parent_id") instanceof Boolean)){
                Object[] parentIdObj = ( Object[])map.get("parent_id");
                if(parentIdObj != null){
                    parentId = (Integer)parentIdObj[0];
                }
            }
            Integer sRank = (Integer)map.get("supplier_rank");
            String os = (String)map.get("ref");
            if(os != null && !os.isEmpty()){
                if(cRank == 1 || parentId != -1)
                    hm.add(os);
                else if(sRank == 1)
                    hm2.add(os);
            }

        }
        return Pair.of(hm,hm2);
    }




    public static ConcurrentHashMap<String, List<CustomerAddress>>   loadCustomersFromOdoo() throws XmlRpcException, MalformedURLException {
        ConcurrentHashMap<String, List<CustomerAddress>> hm = new ConcurrentHashMap<String, List<CustomerAddress>>();
        XmlRpcClient models = HelperFunctions.getModelConfig();
        List<Object> result = asList((Object[]) models.execute("execute_kw", asList(
                HelperFunctions.SERVER, HelperFunctions.authenticateToOdoo(), HelperFunctions.PASSWORD,
                "res.partner", "search_read",
               /* asList(asList(
                        asList("id", ">", 280)
                )), */
               Collections.emptyList(),
                new HashMap() {{
                    put("fields", asList(
                            "id",
                            "country_id",
                            "state_id",
                            "type",
                            "is_company",
                            "ref",
                            "company_type",
                            "name",
                            "phone",
                            "email",
                            "zip",
                            "city",
                            "street",
                            "street2"
                    ));
                }}
        )));

        for(Object o : result){
            HashMap map = (HashMap)o;
            Object ot = map.get("ref");
            if(!(ot instanceof String)){
                System.out.println("Ref no string " + map.get("id"));
                continue;
            }
            String customerId = (String)ot;
            if(customerId != null){
                boolean isContact = false;
                if(customerId.contains("-")){
                    customerId = customerId.substring(0,customerId.indexOf("-"));
                } else {
                    isContact = true;
                }
                if(!hm.containsKey(customerId)){
                    hm.put(customerId, new ArrayList<>());
                }

                CustomerAddress address = new CustomerAddress();
                address.setStreet(getValue(map,"street"));
                address.setStreet(getValue(map,"street2"));
                address.setCity(getValue(map,("city")));
                address.setZip(getValue(map,("zip")));
                address.setEmail(getValue(map,("email")));
                address.setTel(getValue(map,("phone")));
                address.setName(getValue(map,("name")));
                address.setCountry(getValue(map,("country_id")));
                address.setState(getValue(map,("state_id")));
                address.setName(getValue(map,("name")));
                address.setOdooId((Integer) map.get("id"));
                address.setContact(isContact);
                hm.get(customerId).add(address);

            }
        }
        return hm;
    }

    public static String getValue(HashMap map, String key){
        Object street2 = map.get(key);
        if(street2 instanceof String)
            return (String)map.get(key);
        if(street2 instanceof Boolean)
            return "";
        if(street2 instanceof Object[]) {
            switch (key) {
                case "state_id":
                case "country_id":
                    return ((Integer)((Object[])street2)[0]).toString();
                default:
                    return "";
            }
        }
        else {
            return "";
        }
    }

    public static Integer tryParse(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static Map<Integer,Integer> getLineIds(int uid, int parentId) throws MalformedURLException, XmlRpcException {
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "sale.order.line", "search_read",
                        asList(asList(
                                asList("order_id", "=", parentId))),
                        new HashMap<>() {{
                            put("fields", asList("id","product_id"));
                        }})));
        Map<Integer,Integer> i = new HashMap<>();
        for(Object o : ids){
            HashMap m = (HashMap)o;
            i.put((Integer)((Object[])m.get("product_id"))[0], (Integer) m.get("id"));
        }
        return i;
    }

    public static HashMap<String,Integer> createFiscalLookup(int uid) throws MalformedURLException, XmlRpcException {
        HashMap<String,Integer> unitsOfMeasure = new HashMap<>();
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "account.fiscal.position", "search_read",
                        Collections.emptyList(),
                        new HashMap<>() {{
                            put("fields", asList("id","name","country_id","state_ids"));
                        }})));

        if (ids.isEmpty())
            return new HashMap<>();
        for(Object o : ids){
            HashMap map = (HashMap)o;
            Object[] stateIds = ((Object[])map.get("state_ids"));
            if(stateIds.length > 0){
                Integer stateId = (Integer)stateIds[0];
                unitsOfMeasure.put(stateId.toString(),(Integer)map.get("id"));
            } else {

                unitsOfMeasure.put((String)map.get("name"),(Integer)map.get("id"));
            }

            //unitsOfMeasure.put((Integer)cId[0],(Integer)map.get("id"));
        }
        return unitsOfMeasure;
    }


    public static void createInvoice(Order p, Map<String, Object> ord, int parentId, int accountId, int uid) throws MalformedURLException, XmlRpcException {
           /*
invoice_vals = {
            'type': 'out_invoice',
            'invoice_origin': order.name,
            'invoice_user_id': order.user_id.id,
            'narration': order.note,
            'partner_id': order.partner_invoice_id.id,
            'fiscal_position_id': order.fiscal_position_id.id or order.partner_id.property_account_position_id.id,
            'partner_shipping_id': order.partner_shipping_id.id,
            'currency_id': order.pricelist_id.currency_id.id,
            'invoice_payment_ref': order.client_order_ref,
            'invoice_payment_term_id': order.payment_term_id.id,
            'invoice_partner_bank_id': order.company_id.partner_id.bank_ids[:1],
            'team_id': order.team_id.id,
            'campaign_id': order.campaign_id.id,
            'medium_id': order.medium_id.id,
            'source_id': order.source_id.id,
            'invoice_line_ids': [(0, 0, {

            })],
        }
  */

           HashMap<String,Object> contact = new HashMap<>();

        contact.put("date",ord.get("date_order"));
        contact.put("type","out_invoice");
        contact.put("invoice_origin",ord.get("name"));
        contact.put("partner_id",ord.get("partner_id"));
        contact.put("partner_shipping_id",ord.get("partner_shipping_id"));

        Map<Integer,Integer> ids = getLineIds(uid,parentId);

        List<List<Object>> l = new ArrayList<>();
        for(List<Object> it : ((List<List<Object>>)ord.get("order_line"))){
            HashMap i = (HashMap)it.get(2);
/*


                'product_id': self.product_id.id,
                'product_uom_id': so_line.product_uom.id,
                'tax_ids': [(6, 0, so_line.tax_id.ids)],
                'sale_line_ids': [(6, 0, [so_line.id])],
                'analytic_tag_ids': [(6, 0, so_line.analytic_tag_ids.ids)],
                'analytic_account_id': order.analytic_account_id.id or False,
             */
            HashMap<String,Object> t = new HashMap<>();
            t.put("product_id",i.get("product_id"));
            //t.put("product_uom_id",i.get("product_uom_id"));
            t.put("price_unit",i.get("price_unit"));
            t.put("quantity",i.get("product_uom_qty"));
            t.put("analytic_account_id",accountId);
            t.put("sale_line_ids",asList(6,0,asList(ids.get((Integer)i.get("product_id")))));
            //t.put("amount_currency",5);
            l.add(asList(0,0,t));
        }

        contact.put("invoice_line_ids",l);

        int id = -1;
        try {
            id = (int) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                    "account.move", "create",
                    Collections.singletonList(contact)
            ));
        } catch (XmlRpcException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
