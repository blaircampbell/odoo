package com.binnacle;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.tinylog.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import reactor.util.concurrent.Queues;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Customer {
    public static ExecutorService exService = Executors.newCachedThreadPool();
    private String id;
    private String name;
    private String credit;
    private List<CustomerAddress> addressList;
    private Integer poRequired;
    private String termsCode;

    public Customer() {
        addressList = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public List<CustomerAddress> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<CustomerAddress> addressList) {
        this.addressList = addressList;
    }

    public String isCompany() {
        return Boolean.toString(!containsDigit(getId()));
    }

    public final boolean containsDigit(String s) {
        boolean containsDigit = false;
        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }
        return containsDigit;
    }

    public String setCountry(CustomerAddress billing) {
        switch (billing.getCountry().toLowerCase()) {
            case "dnk":
                return "dk";
            case "pol":
                return "pl";
            case "ita":
                return "it";

            case "fin":
                return "fi";

            case "vgb":
                return "vg";

            case "grd":
                return "gd";

            case "aus":
                return "au";

            case "glp":
                return "gp";

            case "cym":
                return "ky";

            case "atg":
                return "ag";

            case "spm":
                return "pm";

            case "lca":
                return "lc";

            case "chl":
                return "cl";

            case "nzl":
                return "nz";

            case "mus":
                return "mu";

            case "mex":
                return "mx";

            case "bel":
                return "be";

            case "nor":
                return "no";

            case "khm":
                return "kh";

            case "che":
                return "ch";

            case "ury":
                return "uy";

            case "tto":
                return "tt";

            case "vir":
                return "vi";

            case "deu":
                return "de";

            case "vnm":
                return "vn";

            case "mys":
                return "my";

            case "bhs":
                return "bs";

            case "aut":
                return "at";

            case "gib":
                return "gi";

            case "jpn":
                return "jp";

            case "nld":
                return "nl";

            case "isl":
                return "is";

            case "grc":
                return "gr";

            case "cdn":
                return "ca";

            case "usa":
                return "us";
            case "fra":
                return "fr";

            case "tcd":
                return "td";

            case "esp":
                return "es";

            case "uga":
                return "ug";

            case "bmu":
                return "bm";

            case "arg":
                return "ar";
            case "gbr":
                return "gb";
            default:
                System.out.println("Unknown Country: " + billing.getCountry());
                System.out.println("Unknown State: " + billing.getState());
        }
        return null;
    }


    public static boolean addressEquals(CustomerAddress a, CustomerAddress b) {
        if (b.getZip() != null && a.getZip() != null && b.getZip().equalsIgnoreCase(a.getZip())
                && ((a.getName() != null && b.getName() != null && a.getName().equalsIgnoreCase(b.getName())) || (a.getName() == null && b.getName() == null))) {
            return true;
        } else if ((a.getName() == null && b.getName() == null || (a.getName() != null && b.getName() != null && a.getName().equalsIgnoreCase(b.getName())))
                && (b.getStreet2() == null && a.getStreet2() == null || b.getStreet2() != null && a.getStreet2() != null && b.getStreet2().equalsIgnoreCase(a.getStreet2()))
                && (b.getStreet() == null && a.getStreet() == null || b.getStreet() != null && a.getStreet() != null && b.getStreet().equalsIgnoreCase(a.getStreet()))) {
            return true;
        } else {
            return false;
        }
    }


    public boolean doesAddressAlreadyExist(CustomerAddress a) {
        for (CustomerAddress ca : this.getAddressList()) {
            boolean match = Customer.addressEquals(ca, a);
            if (match)
                return true;
        }
        return false;
    }

    public Queue<CustomerAddress> getMap() {
        if (this.getId() == null || this.getId().isEmpty() || this.getId().toLowerCase().contains("customer"))
            return null;
        Queue<CustomerAddress> ret = new LinkedList<>();
        CustomerAddress billing = null;
        for (CustomerAddress a : getAddressList()) {
            if (a.getType().equalsIgnoreCase("B")) {
                billing = a;
                break;
            }
        }

        if (billing == null) {
            for (CustomerAddress a : getAddressList()) {
                if (a.getType().equalsIgnoreCase("S")) {
                    billing = a;
                    break;
                }
            }
        }

        //Create Contact Customer
        if (billing != null) {
            billing.setContact(true);
            ret.add(billing);
            int i = 0;
            for (CustomerAddress a : getAddressList()) {
                if (a != billing) {
                    i+=1;
                    billing = a;
                    ret.add(billing);
                    /*HashMap<String, Object> otherContact = new HashMap<>();
                    otherContact.put("name", getName());
                    otherContact.put("parent_id", "");
                    otherContact.put("street", billing.getStreet());
                    otherContact.put("street2", billing.getStreet2());
                    otherContact.put("city", billing.getCity());
                    otherContact.put("zip", billing.getZip());
                    otherContact.put("email", billing.getEmail());
                    otherContact.put("phone", billing.getTel());
                    otherContact.put("ref",getId() + "-"  + i);
                    String otherCountry = setCountry(billing);
                    if(otherCountry != null){
                        Integer otherCid = HelperFunctions.countryToId.get(otherCountry.toLowerCase());
                        if(otherCid != null){
                            otherContact.put("country_id", otherCid);
                        }
                        if (otherCountry.equalsIgnoreCase("usa") || otherCountry.equalsIgnoreCase("ca")) {
                            Integer state = HelperFunctions.stateLook.get(Pair.of(billing.getState().toLowerCase(),otherCid));
                            if(state != null)
                                otherContact.put("state_id", state);
                            else
                                System.out.println("State Null " + country + " " + billing.getState());
                        }
                        otherContact.put("type", "delivery");
                    }*/


                }
            }

        }
        return ret;
    }

    public void setPoNumberRequired(Integer poRequired) {
        this.poRequired = poRequired;
    }

    public boolean getPoNumberRequired(){
        return this.poRequired == 1;
    }

    public void setTermsCode(String term_code) {
        this.termsCode = term_code;
    }

    public String getTermsCode(){
        return this.termsCode;
    }
}