package com.binnacle;

import io.vavr.Tuple;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcHttpTransportException;
import org.jline.utils.InfoCmp;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RKeys;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Arrays.asList;


public class ImportMain {


    static {
        InfoCmp.setDefaultInfoCmp("dumb-color", () -> {
            try {
                return new String(IOUtils.toByteArray(InfoCmp.class.getResourceAsStream("/org/jline/utils/dumb-colors.caps")));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:pervasive://BINNACLE-SERVER:1583/Binnacle", "server", "bin6464");
    }

    public static void createModule(List<String> files, String name, String dep) throws IOException {
        String newFolder = String.format("C:\\Users\\blair\\Documents\\the-binnacle\\%s", name);
        File manifest = new File("C:\\Users\\blair\\Documents\\the-binnacle\\manifest");
        File init = new File("C:\\Users\\blair\\Documents\\the-binnacle\\init");

        File folder = new File(String.format("%s\\data", newFolder));
        if (folder.exists()) {
            folder.delete();
        }
        folder.mkdirs();

        Path path = Paths.get(manifest.getPath());
        Charset charset = StandardCharsets.UTF_8;

        String content = new String(Files.readAllBytes(path), charset);
        content = content.replaceAll("%name%", name);
        content = content.replaceAll("%dep%", dep);
        StringBuilder builder = new StringBuilder();

        for (String file : files) {
            File f = new File(file);
            builder.append("'data/" + f.getName() + "',");
            Files.move(Path.of(file), Path.of(folder.getPath(), f.getName()), REPLACE_EXISTING);
        }

        content = content.replaceAll("%token%", builder.substring(0, builder.length() - 1));
        Files.write(Path.of(newFolder, "__manifest__.py"), content.getBytes(charset));
        Files.copy(Path.of(init.getPath()), Path.of(newFolder, "__init__.py"), REPLACE_EXISTING);
    }

    public static ConcurrentHashMap<String, List<CustomerAddress>> doVendors(ConcurrentHashMap<String, List<CustomerAddress>> customer) throws TransformerException, ParserConfigurationException, IOException, XmlRpcException, InterruptedException {
        Config config = new Config();
        config.useSingleServer()
                .setDatabase(8)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient customerConnection = Redisson.create(config);

        Config addressConnectionC = new Config();
        addressConnectionC.useSingleServer()
                .setDatabase(9)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient addressConnection = Redisson.create(addressConnectionC);

        RKeys keys = customerConnection.getKeys();

        ConcurrentLinkedQueue<Customer> queue = new ConcurrentLinkedQueue<>();

        for (String k : keys.getKeys()) {
            RBucket<List<Map<String, Object>>> bucket = customerConnection.getBucket(k);
            Map<String, Object> customerMap = bucket.get().get(0);
            Customer item = new Customer();
            //item.setCredit((String) customerMap.get("CREDIT_LINE"));
            item.setId(((String) customerMap.get("VEN_NO")).strip());
            item.setName(((String) customerMap.get("NAME")).strip());
            RBucket<List<Map<String, Object>>> bucket2 = addressConnection.getBucket(k);
            List<Map<String, Object>> customerMap2 = bucket2.get();
            int aId = 0;
            for (Map<String, Object> addMap : customerMap2) {
                aId += 1;
                CustomerAddress ca = new CustomerAddress();
                ca.setCustomerId(Integer.toString(aId));
                ca.setName(((String) addMap.get("NAME")).strip());
                ca.setCity(((String) addMap.get("BVCITY")).strip());
                ca.setEmail(((String) addMap.get("BVADDREMAIL")).strip());
                ca.setStreet(((String) addMap.get("BVADDR1")).strip());
                ca.setStreet2(((String) addMap.get("BVADDR2")).strip());
                ca.setTel(((String) addMap.get("BVADDRTELNO1")).strip());
                ca.setTel2(((String) addMap.get("BVADDRTELNO2")).strip());
                ca.setType(((String) addMap.get("ADDR_TYPE")).strip());
                ca.setZip(((String) addMap.get("BVPOSTALCODE")).strip());
                ca.setCountry(((String) addMap.get("BVCOUNTRYCODE")).strip());
                ca.setState(((String) addMap.get("BVPROVSTATE")).strip());
                item.getAddressList().add(ca);
            }
            queue.add(item);
        }

        CountDownLatch latch = new CountDownLatch(5);
        ConcurrentHashMap<String, List<CustomerAddress>> addresses = new ConcurrentHashMap<>();

        ExecutorService serv = Executors.newCachedThreadPool();
        int uid = HelperFunctions.authenticateToOdoo();

        for (int x = 0; x < 5; x++) {
            int finalX = x;
            serv.submit(() -> {
                while (!queue.isEmpty()) {
                    Customer p = queue.poll();


                    Queue<CustomerAddress> newQueue = p.getMap();
                    if (newQueue == null)
                        continue;
                    int parentId = -1;
                    while (!newQueue.isEmpty()) {
                        try {
                            if(customer.containsKey(p.getId())){
                                Optional<CustomerAddress> address = customer.get(p.getId()).stream().filter(c -> c.isContact()).findFirst();
                                if(!address.isEmpty()){
                                    CustomerAddress existingAddress = address.get();
                                    HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                            HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                            "res.partner", "write",
                                            asList(
                                                    asList(existingAddress.getOdooId()),
                                                    new HashMap() {{ put("supplier_rank", 1); }}
                                            )
                                    ));
                                    newQueue.remove();
                                }
                            }  else {
                                CustomerAddress address = newQueue.peek();
                                HashMap<String, Object> contact = address.getMap();


                                if (address.isContact()) {
                                    contact.put("name", p.getName());
                                    contact.put("company_type", "person");
                                    contact.put("ref", p.getId());
                                    contact.put("is_company", "true");
                                    contact.put("type", "contact");
                                    contact.put("supplier_rank", "1");
                                    parentId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                            HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                            "res.partner", "create",
                                            Collections.singletonList(contact)
                                    ));
                                    address.setContact(true);
                                    address.setOdooId(parentId);
                                    //contact.put("odoo-id",parentId);
                                } else {

                                    contact.put("parent_id", parentId);
                                    contact.put("name", p.getName());
                                    contact.put("ref", p.getId() + "-" + newQueue.size());
                                    contact.put("type", "delivery");

                                    int localId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                            HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                            "res.partner", "create",
                                            Collections.singletonList(contact)
                                    ));
                                    address.setOdooId(localId);
                                }


                                if (!addresses.containsKey(p.getId())) {
                                    addresses.put(p.getId(), new ArrayList<>());
                                }
                                addresses.get(p.getId()).add(address);
                                // System.out.println( finalX + " "  + "\t" + p.getId() + " " + newQueue.size());
                                newQueue.remove();
                            }


                        } catch (XmlRpcException e) {
                            if (e instanceof XmlRpcHttpTransportException) {
                                XmlRpcHttpTransportException ex = (XmlRpcHttpTransportException) e;
                                if (ex.getStatusCode() == 429 || ex.getStatusCode() == 500) {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex2) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    e.printStackTrace();
                                }
                            } else {
                                if (!e.getMessage().contains("The Connection Pool Is Full"))
                                    e.printStackTrace();
                            }

                        } catch (MalformedURLException e) {

                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("!!!DONE " + latch.getCount() + " " + finalX);
                latch.countDown();
            });
        }

        latch.await();
        return addresses;

    }



    public static CustomerAddress fixAddress(final CustomerAddress address) throws IOException {
        CustomerAddress local = address;

        List<String> validCanadianProv = asList("AB","BC","NS","NB","ON","YK","YT","PE","MB","NL","NT","NU","QC","SK");
        List<String> validUsStates = asList("AE","AP","VI","PR","AS","DC","AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");
        if(address.getCountry() != null ){
            String country = address.getCountry().toUpperCase();
            if(address.getState().equalsIgnoreCase("NW")){
                address.setState("NT");
            }
            if(address.getState().equalsIgnoreCase("YK")){
                address.setState("YT");
            }
            switch(country){
                case "CDN":
                    String prov = address.getState();
                    String postalCode = address.getZip();
                    if(validCanadianProv.contains(prov)){

                    } else if(prov != null){
                         if(validUsStates.contains(prov)){
                            //valid us state, set country to USA
                             address.fixed = true;
                             address.setCountry("USA");
                        } else if(postalCode != null && HelperFunctions.tryParse(postalCode) != null){
                            //since this string is an integer we are in a US state without the state being selected
                             String state = HelperFunctions.getStateFromZipCode(3,postalCode);
                             if(state != null){
                                 address.fixed = true;
                                 address.setCountry("USA");
                                 address.setState(state.toUpperCase());
                             }
                         } else {
                             int a = 10;
                         }
                    } else {
                        int a = 10;
                    }
                    break;
                case "USA":
                    String state = address.getState();
                    String zipCode = address.getZip();
                    if(validUsStates.contains(state)){
                    } else if(state != null){
                        if(validCanadianProv.contains(state)){
                            address.fixed = true;
                            address.setCountry("CDN");
                        } else if(zipCode != null){
                            String foundState = HelperFunctions.getStateFromZipCode(3,zipCode);
                            if(foundState != null){
                                address.fixed = true;
                                address.setState(foundState);
                            }
                        }
                    } else {
                        int a  = 10;
                    }
                    break;
                default:
                    String other = address.getState();
                    if(other != null) {
                        if (validCanadianProv.contains(other)) {
                            address.fixed = true;
                            address.setCountry("CDN");
                        } else if (validUsStates.contains(other)) {
                            address.fixed = true;
                            address.setCountry("USA");
                        } else {
                            switch(address.getCountry().toUpperCase()){
                                default:
                                    break;
                            }
                        }
                    }
            }
        }

        return local;
    }

    public static ConcurrentHashMap<String, List<CustomerAddress>> doCustomers() throws ParserConfigurationException, TransformerException, IOException, XmlRpcException, InterruptedException {

        Config config = new Config();
        config.useSingleServer()
                .setDatabase(0)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient customerConnection = Redisson.create(config);

        Config addressConnectionC = new Config();
        addressConnectionC.useSingleServer()
                .setDatabase(1)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient addressConnection = Redisson.create(addressConnectionC);

        RKeys keys = customerConnection.getKeys();

        ConcurrentLinkedQueue<Customer> queue = new ConcurrentLinkedQueue<>();

        for (String k : keys.getKeys()) {
            RBucket<List<Map<String, Object>>> bucket = customerConnection.getBucket(k);
            Map<String, Object> customerMap = bucket.get().get(0);
            Customer item = new Customer();
            //item.setCredit((String) customerMap.get("CREDIT_LINE"));
            item.setId(((String) customerMap.get("CUS_NO")).strip());
            item.setName(((String) customerMap.get("NAME")).strip());
            item.setPoNumberRequired(((Integer) customerMap.get("PO_NO_REQUIRED")));
            item.setTermsCode(((String) customerMap.get("TERM_CODE")).strip());

            RBucket<List<Map<String, Object>>> bucket2 = addressConnection.getBucket(k);
            List<Map<String, Object>> customerMap2 = bucket2.get();
            int aId = 0;
            for (Map<String, Object> addMap : customerMap2) {
                aId += 1;
                CustomerAddress ca = new CustomerAddress();
                ca.setCustomerId(Integer.toString(aId));
                ca.setName(((String) addMap.get("NAME")).strip());
                ca.setCity(((String) addMap.get("BVCITY")).strip());
                ca.setEmail(((String) addMap.get("BVADDREMAIL")).strip());
                ca.setStreet(((String) addMap.get("BVADDR1")).strip());
                ca.setStreet2(((String) addMap.get("BVADDR2")).strip());
                ca.setTel(((String) addMap.get("BVADDRTELNO1")).strip());
                ca.setTel2(((String) addMap.get("BVADDRTELNO2")).strip());
                ca.setType(((String) addMap.get("ADDR_TYPE")).strip());
                ca.setZip(((String) addMap.get("BVPOSTALCODE")).strip());
                ca.setCountry(((String) addMap.get("BVCOUNTRYCODE")).strip());
                ca.setState(((String) addMap.get("BVPROVSTATE")).strip());
                ca = fixAddress(ca);
                if(!item.doesAddressAlreadyExist(ca)){
                    item.getAddressList().add(ca);
                } else {
                    System.out.println("ALREADY HAD ADDRESS");
                }
            }
            queue.add(item);
        }

        ExecutorService serv = Executors.newCachedThreadPool();
        int uid = HelperFunctions.authenticateToOdoo();

        CountDownLatch latch = new CountDownLatch(5);
        ConcurrentHashMap<String, List<CustomerAddress>> addresses = new ConcurrentHashMap<>();

        for (int x = 0; x < 5; x++) {
            int finalX = x;
            serv.submit(() -> {
                while (!queue.isEmpty()) {
                    Customer p = queue.poll();
                    Queue<CustomerAddress> newQueue = p.getMap();
                    if (newQueue == null)
                        continue;
                    int parentId = -1;
                    while (!newQueue.isEmpty()) {
                        try {
                            CustomerAddress address = newQueue.peek();
                            HashMap<String, Object> contact = address.getMap();
                            if (address.isContact()) {
                                if(contact.containsKey("state_id")){
                                    Integer stateId = (Integer) contact.get("state_id");
                                    Integer countryId = (Integer) contact.get("country_id");
                                    if(HelperFunctions.stateToFiscalPosition.containsKey(stateId.toString())){
                                        contact.put("property_account_position_id", HelperFunctions.stateToFiscalPosition.get(stateId.toString()));
                                    } else if(countryId.toString().equals("233")){
                                        contact.put("property_account_position_id", HelperFunctions.stateToFiscalPosition.get("United States"));
                                    } else {
                                        contact.put("property_account_position_id", HelperFunctions.stateToFiscalPosition.get("United States"));
                                    }
                                } else {
                                    contact.put("property_account_position_id", HelperFunctions.stateToFiscalPosition.get("United States"));
                                }


                                Integer termsCode = HelperFunctions.getOdooPaymentTerms(p.getTermsCode());
                                if(termsCode != -1){
                                    contact.put("property_payment_term_id",termsCode);
                                }

                                contact.put("x_studio_purchase_order_required_1", p.getPoNumberRequired());
                                contact.put("name", p.getName());
                                contact.put("company_type", "person");
                                contact.put("ref", p.getId());
                                contact.put("is_company", p.isCompany());
                                contact.put("type", "contact");
                                contact.put("customer_rank", "1");
                                if(address.fixed)
                                    contact.put("comment","Odoo Import Fixed This Address");
                                parentId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                        "res.partner", "create",
                                        Collections.singletonList(contact)
                                ));
                                address.setContact(true);
                                address.setOdooId(parentId);
                                //contact.put("odoo-id",parentId);
                            } else {

                                contact.put("parent_id", parentId);
                                contact.put("name", p.getName());
                                contact.put("ref", p.getId() + "-" + newQueue.size());
                                contact.put("type", "delivery");
                                if(address.fixed)
                                    contact.put("comment","Odoo Import Fixed This Address");
                                int localId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                        "res.partner", "create",
                                        Collections.singletonList(contact)
                                ));
                                address.setOdooId(localId);
                            }
                            newQueue.remove();
                        } catch (XmlRpcException e) {
                            if (e instanceof XmlRpcHttpTransportException) {
                                XmlRpcHttpTransportException ex = (XmlRpcHttpTransportException) e;
                                if (ex.getStatusCode() == 429 || ex.getStatusCode() == 500) {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex2) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    e.printStackTrace();
                                }
                            } else {
                                if (!e.getMessage().contains("The Connection Pool Is Full"))
                                    e.printStackTrace();
                            }

                        } catch (MalformedURLException e) {

                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("!!!DONE " + latch.getCount() + " " + finalX);
                latch.countDown();
            });
        }
        latch.await();
        return addresses;
    }

    public static void doProducts(HashMap<String, Object> currentMeasures, Map<String, Object> binMeasures) throws ParserConfigurationException, TransformerException, IOException, XmlRpcException, ExecutionException, InterruptedException {
        Config productConfig = new Config();
        productConfig.useSingleServer()
                .setDatabase(4)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient productConnection = Redisson.create(productConfig);

        Config barCodeConfig = new Config();
        barCodeConfig.useSingleServer()
                .setDatabase(2)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient barcodeConnection = Redisson.create(barCodeConfig);


        Config pricingConfig = new Config();
        pricingConfig.useSingleServer()
                .setDatabase(3)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient pricingConnection = Redisson.create(pricingConfig);

        RKeys keys = productConnection.getKeys();

        ConcurrentLinkedQueue<Product> queue = new ConcurrentLinkedQueue<>();

        Set<String> barcodes = new HashSet<>();
        for (String k : keys.getKeys()) {
            RBucket<List<Map<String, Object>>> bucket = productConnection.getBucket(k);
            Map<String, Object> customerMap = bucket.get().get(0);
            Product item = new Product();
            item.setStandardPrice(((BigDecimal) customerMap.get("WEIGHTED")));
            item.setCode(((String) customerMap.get("CODE")).strip());
            item.setDescription(((String) customerMap.get("INV_DESCRIPTION")).strip());
            item.setOnHand(((BigDecimal) customerMap.get("ONHAND")));

            RBucket<List<Map<String, Object>>> bucket2 = barcodeConnection.getBucket(k);
            List<Map<String, Object>> customerMap2 = bucket2.get();
            Map<String, Object> latest = getLatestInList(customerMap2, "BVRVMODDATE");
            String barCode = (String) latest.get("UPC_ALTERNATE");
            if (!barcodes.contains(barCode.toLowerCase().strip()) && !barCode.strip().isEmpty()) {
                barcodes.add(barCode.toLowerCase().strip());
                 item.setBarcode(barCode.toLowerCase().strip());
            }
            item.setUnitOfMeasure((String) latest.get("CODE"));


            RBucket<List<Map<String, Object>>> bucket3 = pricingConnection.getBucket(k);
            List<Map<String, Object>> customerMap3 = bucket3.get();
            Map<String, Object> latest3 = getLatestInList(customerMap3, "BVRVMODDATE");
            item.setListPrice((BigDecimal) latest3.get("BVRTLPRICE01"));
            item.setPriceUnitOfMeasure((String) latest3.get("BVSPECPRICEUOM"));

            queue.add(item);


        }

        ExecutorService serv = Executors.newCachedThreadPool();
        int uid = HelperFunctions.authenticateToOdoo();

        CountDownLatch latch = new CountDownLatch(5);
        for (int x = 0; x < 5; x++) {
            int finalX = x;
            serv.submit(() -> {
                while (!queue.isEmpty()) {
                    Product p = queue.poll();
                    HashMap<String, Object> custMap = p.getMap();

                    Integer uomId = (Integer) ((HashMap) currentMeasures.get("units")).get("id");
                    Integer uomPoId = (Integer) ((HashMap) currentMeasures.get("units")).get("id");
                    if (binMeasures.containsKey(custMap.get("uom_id"))) {
                        Object odooId = binMeasures.get(custMap.get("uom_id"));
                        if (currentMeasures.containsKey(odooId)) {
                            uomId = (Integer) ((HashMap) currentMeasures.get(odooId)).get("id");
                        }
                    }

                    if (binMeasures.containsKey(custMap.get("uom_po_id"))) {
                        Object odooId = binMeasures.get(custMap.get("uom_id"));
                        if (currentMeasures.containsKey(odooId)) {
                            uomPoId = (Integer) ((HashMap) currentMeasures.get(odooId)).get("id");
                        }
                    }

                    custMap.put("uom_id", uomId);
                    custMap.put("uom_po_id", uomPoId);

                    while (true) {
                        try {
                            HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                    HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                    "product.product", "create",
                                    Collections.singletonList(custMap)
                            ));
                            break;
                        } catch (XmlRpcException e) {
                            if (e instanceof XmlRpcHttpTransportException) {
                                XmlRpcHttpTransportException ex = (XmlRpcHttpTransportException) e;
                                if (ex.getStatusCode() == 429 || ex.getStatusCode() == 500) {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex2) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    e.printStackTrace();
                                }
                            } else {
                                e.printStackTrace();
                            }

                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }

                }
                latch.countDown();
            });
        }

        latch.await();
    }

    public static Map<String, Object> getLatestInList(List<Map<String, Object>> list, String key) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
        list.sort((o1, o2) -> {
            try {
                Date o1Date = format1.parse((String) o1.get(key));
                Date o2Date = format1.parse((String) o2.get(key));
                return o2Date.compareTo(o1Date);
            } catch (ParseException e) {
                return 0;
            }
        });

        return list.get(0);
    }

    public static void doOrders(Map<String, Pair<Integer, String>> productMap, Map<String, List<CustomerAddress>> savedCustomerMap) throws ParserConfigurationException, TransformerException, IOException, XmlRpcException, InterruptedException {
        Config orderConfig = new Config();
        orderConfig.useSingleServer()
                .setDatabase(5)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient orderConnection = Redisson.create(orderConfig);

        Config itemConfig = new Config();
        itemConfig.useSingleServer()
                .setDatabase(6)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient itemCon = Redisson.create(itemConfig);


        Config addressConfig = new Config();
        addressConfig.useSingleServer()
                .setDatabase(7)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient addressCon = Redisson.create(addressConfig);


        RKeys keys = orderConnection.getKeys();

        ConcurrentLinkedQueue<Order> queue = new ConcurrentLinkedQueue<>();

        for (String k : keys.getKeys()) {
            RBucket<List<Map<String, Object>>> bucket = orderConnection.getBucket(k);
            Map<String, Object> customerMap = bucket.get().get(0);
            Order item = new Order();
            item.setOrderId(((String) customerMap.get("NUMBER")).strip());
            item.setCustomerId(((String) customerMap.get("CUST_NO")).strip());
            item.setCustomerPoNumber(((String) customerMap.get("CUST_PO_NO")).strip());
            item.setPoNumber(((String) customerMap.get("PO_NO")).strip());
            item.setTermsCode(((String) customerMap.get("BVTERMSINFOCODE")).strip());
            item.setOrderDate(((String) customerMap.get("SH_DATE")).strip());
            item.setShippingCost((BigDecimal) customerMap.get("BVSHIPPING"));
            System.out.println("Order " + k);
            String posUserId = ((String) customerMap.get("POS_USER_ID")).replace("\u0000", "");
            if (posUserId != null && !posUserId.strip().isEmpty())
                item.setPosOrder(true);


            RBucket<List<Map<String, Object>>> bucket2 = itemCon.getBucket(k);
            List<Map<String, Object>> customerMap2 = bucket2.get();
            if (customerMap2 == null) {
                System.out.println("Order has no items " + item.getOrderId() + " " + item.getCustomerId());
                continue;
            }
            for (Map<String, Object> itemMap : customerMap2) {
                OrderItem item2 = new OrderItem();
                item2.setProductDescription((String) itemMap.get("SHD_DESCRIPTION"));
                item2.setOrderId((String) itemMap.get("NUMBER"));
                item2.setPartCode((String) itemMap.get("CODE"));
                item2.setProductCode((String) itemMap.get("PROD_CODE"));
                item2.setUnitOfMeasure((String) itemMap.get("BVBUYUOM"));
                item2.setQuantity((BigDecimal) itemMap.get("BVORDQTY"));
                item2.setCommitedQuantity((BigDecimal) itemMap.get("BVCMTDQTY"));
                item2.setBackOrderQuantity((BigDecimal) itemMap.get("BVBOQTY"));
                item2.setUnitPrice((BigDecimal) itemMap.get("BVUNITPRICE"));
                item2.setWarehouse((String) itemMap.get("WHSE"));
                item2.setOrderId((String) itemMap.get("NUMBER"));
                item.getOrderItems().add(item2);
            }
            System.out.println("Order Items" + k);


            RBucket<List<Map<String, Object>>> bucket3 = addressCon.getBucket(k.strip() + "          ");
            List<Map<String, Object>> customerMap3 = bucket3.get();
            List<CustomerAddress> addressLookup = savedCustomerMap.get(item.getCustomerId());
            if (addressLookup != null && customerMap3 != null) {
                for (Map<String, Object> itemMap : customerMap3) {
                    CustomerAddress ca = new CustomerAddress();
                    ca.setCustomerId(((String) itemMap.get("CEV_NO")).strip());
                    ca.setName(((String) itemMap.get("NAME")).strip());
                    ca.setCity(((String) itemMap.get("BVCITY")).strip());
                    ca.setEmail(((String) itemMap.get("BVADDREMAIL")).strip());
                    ca.setStreet(((String) itemMap.get("BVADDR1")).strip());
                    ca.setStreet2(((String) itemMap.get("BVADDR2")).strip());
                    ca.setTel(((String) itemMap.get("BVADDRTELNO1")).strip());
                    ca.setTel2(((String) itemMap.get("BVADDRTELNO2")).strip());
                    ca.setType(((String) itemMap.get("ADDR_TYPE")).strip());
                    ca.setZip(((String) itemMap.get("BVPOSTALCODE")).strip());
                    ca.setCountry(((String) itemMap.get("BVCOUNTRYCODE")).strip());
                    ca.setState(((String) itemMap.get("BVPROVSTATE")).strip());

                    boolean found = false;
                    for (CustomerAddress custAddress : addressLookup) {
                        if (Customer.addressEquals(ca, custAddress)) {
                            ca.setCustomerId(custAddress.getCustomerNumber());
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        ca.setCustomerId("0");
                    }

                    if (ca.getType().equalsIgnoreCase("s")) {
                        item.setShippingAddress(ca);
                    }

                    if (ca.getType().equalsIgnoreCase("b")) {
                        item.setInvoiceAddress(ca);
                    }

                }
            } else {
                int a = 10;
            }

            System.out.println("Order Address" + k);

            queue.add(item);
            if(queue.size() == 10000)
                break;
        }


        ExecutorService serv = Executors.newCachedThreadPool();
        CountDownLatch latch = new CountDownLatch(5);
        int uid = HelperFunctions.authenticateToOdoo();
        for (int x = 0; x < 5; x++) {
            int finalX = x;
            serv.submit(() -> {
                while (!queue.isEmpty()) {
                    Order p = queue.poll();
                    Queue<Map<String, Object>> newQueue = null;
                    try {
                        newQueue = p.getMap(uid,productMap, savedCustomerMap);
                    } catch (XmlRpcException | IOException e) {
                        System.out.println("Error getting map for " + p.getOrderId());
                    }
                    if (newQueue == null)
                        continue;
                    int parentId = -1;
                    while (!newQueue.isEmpty()) {
                        try {
                            Map<String, Object> contact = newQueue.peek();
                            if (contact.containsKey("date_order")) {
                                // orderId, String customerId, Integer partnerId,
                               // int account = HelperFunctions.createAccount((String)contact.get("name"),p.getCustomerId(),(Integer)contact.get("partner_id"),uid);
                                parentId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                        "sale.order", "create",
                                        Collections.singletonList(contact)
                                ));
                                if (parentId == -1)
                                    continue;
                                //HelperFunctions.createInvoice(p,contact,parentId,account, uid);
                            } else {

                                contact.put("order_id", parentId);
                                int localId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                        "sale.order.line", "create",
                                        Collections.singletonList(contact)
                                ));
                            }
                            newQueue.remove();
                        } catch (XmlRpcException e) {
                            if (e instanceof XmlRpcHttpTransportException) {
                                XmlRpcHttpTransportException ex = (XmlRpcHttpTransportException) e;
                                if (ex.getStatusCode() == 429 || ex.getStatusCode() == 500) {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex2) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    e.printStackTrace();
                                }
                            } else {
                                if (!e.getMessage().contains("The Connection Pool Is Full"))
                                    e.printStackTrace();
                            }

                        } catch (MalformedURLException e) {

                            e.printStackTrace();
                        }
                    }
                    System.out.println(finalX + " \t" + p.getOrderId() + " DONE");


                }
                latch.countDown();
            });
        }

        latch.await();
    }

    public static Element getDirectChild(Element parent, String name) {
        for (Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child instanceof Element && name.equals(child.getNodeName())) return (Element) child;
        }
        return null;
    }

    public static void main(String[] args) throws XmlRpcException, IOException, InterruptedException, ParserConfigurationException, TransformerException, ExecutionException {
        try {
            Class.forName("com.pervasive.jdbc.v2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        boolean load = false;
        if (load) {
            HelperFunctions.loadTableIntoRedis(0, "CUS_NO", "SELECT * FROM CUSTOMER");
            HelperFunctions.loadTableIntoRedis(1, "CEV_NO", "SELECT * FROM ADDRESS WHERE RECORD_TYPE = 'CUST'");
            HelperFunctions.loadTableIntoRedis(2, "PART_NO", "select  * from UNIT_OF_MEASURE");
            HelperFunctions.loadTableIntoRedis(3, "BVSPECPRICEPARTNO", "select  * from PRICING WHERE BVSPECPRICESOURCEID = 'I' and BVSPECPRICEUOM != 'UOM_1'");
            HelperFunctions.loadTableIntoRedis(4, "CODE", "SELECT * FROM INVENTORY");
            HelperFunctions.loadTableIntoRedis(5, "NUMBER", "SELECT * FROM SALES_HISTORY_HEADER");
            HelperFunctions.loadTableIntoRedis(6, "NUMBER", "SELECT LINE_DISC,LINE_DISC_AMT,NUMBER,BVSTATUS,CODE,SHD_DESCRIPTION,BVCURRENTCOSTPRICE,PROD_CODE,BVBUYUOM, BVUNITPRICE,BVSTKUOM,BVORDQTY,BVCMTDQTY,BVBOQTY,BVORDQTY_2,BVCMTDQTY_2,BVBOQTY_2 FROM SALES_HISTORY_DETAIL");
            HelperFunctions.loadTableIntoRedis(7, "CEV_NO", "SELECT * FROM HISTORY_ADDRESS WHERE RECORD_TYPE = 'SHIS'");

            HelperFunctions.loadTableIntoRedis(8, "VEN_NO", "SELECT * FROM VENDOR");
            HelperFunctions.loadTableIntoRedis(9, "CEV_NO", "SELECT * FROM ADDRESS WHERE RECORD_TYPE = 'SUPP'");
            HelperFunctions.loadTableIntoRedis(10, "NUMBER", "SELECT * FROM PURCHASE_HISTORY_HDR");
            HelperFunctions.loadTableIntoRedis(11, "NUMBER", "SELECT LINE_DISC,LINE_DISC_AMT,NUMBER,BVSTATUS,CODE,PHD_DESCRIPTION,BVCURRENTCOSTPRICE,PROD_CODE,BVBUYUOM, BVUNITPRICE,BVSTKUOM,BVORDQTY,BVCMTDQTY,BVBOQTY,BVORDQTY_2,BVCMTDQTY_2,BVBOQTY_2 FROM PURCHASE_HISTORY_DTL");
            HelperFunctions.loadTableIntoRedis(12, "CEV_NO", "SELECT * FROM HISTORY_ADDRESS WHERE RECORD_TYPE = 'PHIS'");

        }


        int uid = HelperFunctions.authenticateToOdoo();


        HelperFunctions.loadExistingCustomer(uid);
        //HelperFunctions.createShippingProduct(uid);
        HashMap<String, Object> currentMeasures = HelperFunctions.getAllUnitsOfMeasure(uid);
        HashMap<String, Object> currentCategories = HelperFunctions.getUnitOfMeasureCategories(uid);
        Map<String, Object> binMeasures = HelperFunctions.setupUnitOfMeasures();

        boolean hasBinValues =  true; //currentMeasures.containsKey("64");

        if (!hasBinValues) {

            Map<String, Object> lookup = binMeasures;
            for (Map.Entry<String, Object> e : lookup.entrySet()) {
                if (e.getValue() instanceof Tuple) {
                    Tuple aRR = (Tuple) e.getValue();
                    Double factor = (Double) aRR.toSeq().get(1);
                    String category = (String) aRR.toSeq().get(0);
                    Integer categoryId = (Integer) ((HashMap) currentCategories.get(category.toLowerCase())).get("id");
                    HashMap<String, Object> custMap = new HashMap<>();
                    custMap.put("uom_type", factor > 1 ? "bigger" : "smaller");
                    custMap.put("category_id", categoryId);
                    custMap.put("factor", factor);
                    custMap.put("name", e.getKey().toLowerCase());

                    HelperFunctions.getModelConfig().execute("execute_kw", asList(
                            HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                            "uom.uom", "create",
                            Collections.singletonList(custMap)
                    ));
                }

            }

        }
        //currentMeasures = HelperFunctions.getAllUnitsOfMeasure(uid);


        System.out.println("doCustomers!");
       // doCustomers();
        ConcurrentHashMap<String, List<CustomerAddress>> customer = HelperFunctions.loadCustomersFromOdoo();


        HelperFunctions.createPosAndUnknownCustomer();

        System.out.println("doProducts!");
        //doProducts(currentMeasures, binMeasures);
        Map<String, Pair<Integer,String>> productMaps = HelperFunctions.loadProductsFromOdoo();

        System.out.println("doOrders!");
        //doOrders(productMaps, customer);

        System.out.println("doVendors!");
        ConcurrentHashMap<String, List<CustomerAddress>> vendor = doVendors(customer);

        System.out.println("doPurchaseOrders!");
        doPurchaseOrders(currentMeasures, binMeasures, productMaps, vendor);

        System.out.println("DONE!");
        return;
    }

    public static void doPurchaseOrders(HashMap<String, Object> currentMeasures, Map<String, Object> binMeasures, Map<String, Pair<Integer, String>> productMap, Map<String, List<CustomerAddress>> savedCustomerMap) throws TransformerException, ParserConfigurationException, IOException, XmlRpcException, InterruptedException {
        Config orderConfig = new Config();
        orderConfig.useSingleServer()
                .setDatabase(10)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient orderConnection = Redisson.create(orderConfig);

        Config itemConfig = new Config();
        itemConfig.useSingleServer()
                .setDatabase(11)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient itemCon = Redisson.create(itemConfig);


        Config addressConfig = new Config();
        addressConfig.useSingleServer()
                .setDatabase(12)
                .setAddress("redis://10.1.2.113:6379");
        RedissonClient addressCon = Redisson.create(addressConfig);

        RKeys keys = orderConnection.getKeys();

        ConcurrentLinkedQueue<Order> queue = new ConcurrentLinkedQueue<>();
        for (String k : keys.getKeys()) {
            RBucket<List<Map<String, Object>>> bucket = orderConnection.getBucket(k);
            Map<String, Object> customerMap = bucket.get().get(0);
            Order item = new Order();
            item.setOrderId(((String) customerMap.get("PO_NUMBER")).strip());
            item.setCustomerId(((String) customerMap.get("VEND_NO")).strip());
            item.setOrderDate(((String) customerMap.get("PH_DATE")).strip());
            item.setReceivedDate(((String) customerMap.get("REQD_DATE")).strip());
            RBucket<List<Map<String, Object>>> bucket2 = itemCon.getBucket(k);
            List<Map<String, Object>> customerMap2 = bucket2.get();
            System.out.println("Order " + k);
            for (Map<String, Object> itemMap : customerMap2) {
                OrderItem orderItem = new OrderItem();

                orderItem.setProductDescription((String) itemMap.get("PHD_DESCRIPTION"));
                orderItem.setOrderId((String) itemMap.get("NUMBER"));
                orderItem.setPartCode((String) itemMap.get("CODE"));
                orderItem.setProductCode((String) itemMap.get("PROD_CODE"));
                orderItem.setUnitOfMeasure((String) itemMap.get("BVBUYUOM"));
                orderItem.setQuantity((BigDecimal) itemMap.get("BVORDQTY"));
                orderItem.setCommitedQuantity((BigDecimal) itemMap.get("BVCMTDQTY"));
                orderItem.setBackOrderQuantity((BigDecimal) itemMap.get("BVBOQTY"));
                orderItem.setUnitPrice((BigDecimal) itemMap.get("BVUNITPRICE"));
                orderItem.setWarehouse((String) itemMap.get("WHSE"));
                orderItem.setOrderId((String) itemMap.get("NUMBER"));
                item.getOrderItems().add(orderItem);
            }

            System.out.println("Order Items " + k);

            RBucket<List<Map<String, Object>>> bucket3 = addressCon.getBucket(k.strip() + "          ");
            List<Map<String, Object>> customerMap3 = bucket3.get();


            String customerId = item.getCustomerId();
            List<CustomerAddress> addressLookup = savedCustomerMap.get(customerId.strip());
            if (addressLookup != null) {
                for (Map<String, Object> itemMap : customerMap3) {
                    CustomerAddress ca = new CustomerAddress();
                    ca.setCustomerId(((String) itemMap.get("CEV_NO")).strip());
                    ca.setName(((String) itemMap.get("NAME")).strip());
                    ca.setCity(((String) itemMap.get("BVCITY")).strip());
                    ca.setEmail(((String) itemMap.get("BVADDREMAIL")).strip());
                    ca.setStreet(((String) itemMap.get("BVADDR1")).strip());
                    ca.setStreet2(((String) itemMap.get("BVADDR2")).strip());
                    ca.setTel(((String) itemMap.get("BVADDRTELNO1")).strip());
                    ca.setTel2(((String) itemMap.get("BVADDRTELNO2")).strip());
                    ca.setType(((String) itemMap.get("ADDR_TYPE")).strip());
                    ca.setZip(((String) itemMap.get("BVPOSTALCODE")).strip());
                    ca.setCountry(((String) itemMap.get("BVCOUNTRYCODE")).strip());
                    ca.setState(((String) itemMap.get("BVPROVSTATE")).strip());

                    boolean found = false;
                    for (CustomerAddress custAddress : addressLookup) {
                        if (Customer.addressEquals(ca, custAddress)) {
                            ca.setCustomerId(custAddress.getCustomerNumber());
                            found = true;
                            break;
                        }
                    }

                    if (!found && ca.getType().equalsIgnoreCase("b")) {
                        ca.setCustomerId("0");
                    } else if (!found && ca.getType().equalsIgnoreCase("s")) {
                        ca.setCustomerId("bin");
                    }

                    if (ca.getType().equalsIgnoreCase("s")) {
                        item.setShippingAddress(ca);
                    }

                    if (ca.getType().equalsIgnoreCase("b")) {
                        item.setInvoiceAddress(ca);
                    }
                }
            } else {
                System.out.println("Can't find customer " + item.getCustomerId() + " " + item.getOrderId());
            }

            System.out.println("Order Addresses " + k);
            queue.add(item);
        }

        ExecutorService serv = Executors.newCachedThreadPool();
        CountDownLatch latch = new CountDownLatch(5);
        int uid = HelperFunctions.authenticateToOdoo();
        for (int x = 0; x < 5; x++) {
            int finalX = x;
            serv.submit(() -> {
                while (!queue.isEmpty()) {
                    Order p = queue.poll();
                    List<CustomerAddress> address = savedCustomerMap.get(p.getCustomerId());
                    if (address == null) {
                        address = savedCustomerMap.get("unknown");
                    }

                    Queue<Map<String, Object>> newQueue = p.getPurchaseMap(currentMeasures, binMeasures, productMap, address);
                    if (newQueue == null)
                        continue;
                    int parentId = -1;
                    while (!newQueue.isEmpty()) {
                        Map<String, Object> contact = null;
                        try {
                             contact = newQueue.peek();
                            if (contact.containsKey("date_order")) {
                                parentId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                        "purchase.order", "create",
                                        Collections.singletonList(contact)
                                ));
                                if (parentId == -1)
                                    continue;
                            } else {
                                contact.put("order_id", parentId);
                                HelperFunctions.getModelConfig().execute("execute_kw", asList(
                                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                                        "purchase.order.line", "create",
                                        Collections.singletonList(contact)
                                ));
                            }
                            newQueue.remove();
                        } catch (XmlRpcException e) {
                            if (e instanceof XmlRpcHttpTransportException) {
                                XmlRpcHttpTransportException ex = (XmlRpcHttpTransportException) e;
                                if (ex.getStatusCode() == 429 || ex.getStatusCode() == 500) {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex2) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    e.printStackTrace();
                                }
                            } else {
                                if (!e.getMessage().contains("The Connection Pool Is Full"))
                                    e.printStackTrace();
                            }

                        } catch (MalformedURLException e) {

                            e.printStackTrace();
                        }
                    }
                    System.out.println(finalX + " \t" + p.getOrderId() + " DONE");


                }
                latch.countDown();
            });
        }
        latch.await();
    }


}
