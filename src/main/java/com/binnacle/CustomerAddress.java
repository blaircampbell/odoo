package com.binnacle;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;

public class CustomerAddress {
    private boolean contact = false;
    private Integer odooId;

    public boolean fixed = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String name;
    private String street;
    private String street2;
    private String city;
    private String state;
    private String zip;
    private String type;
    private String email;
    private String tel;
    private String tel2;
    private String country;
    private String customerNumber;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip == null ? null : zip.replaceAll("\\s+","");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public String setOdooCountry(CustomerAddress billing) {

        switch (billing.getCountry().toLowerCase()) {
            case "dnk":
                return "dk";
            case "pol":
                return "pl";
            case "ita":
                return "it";

            case "fin":
                return "fi";

            case "vgb":
                return "vg";

            case "grd":
                return "gd";

            case "aus":
                return "au";

            case "glp":
                return "gp";

            case "cym":
                return "ky";

            case "atg":
                return "ag";

            case "spm":
                return "pm";

            case "lca":
                return "lc";

            case "chl":
                return "cl";

            case "nzl":
                return "nz";

            case "mus":
                return "mu";

            case "mex":
                return "mx";

            case "bel":
                return "be";

            case "nor":
                return "no";

            case "khm":
                return "kh";

            case "che":
                return "ch";

            case "ury":
                return "uy";

            case "tto":
                return "tt";

            case "vir":
                return "vi";

            case "deu":
                return "de";

            case "vnm":
                return "vn";

            case "mys":
                return "my";

            case "bhs":
                return "bs";

            case "aut":
                return "at";

            case "gib":
                return "gi";

            case "jpn":
                return "jp";

            case "nld":
                return "nl";

            case "isl":
                return "is";

            case "grc":
                return "gr";

            case "cdn":
                return "ca";

            case "usa":
                return "us";
            case "fra":
                return "fr";

            case "tcd":
                return "td";

            case "esp":
                return "es";

            case "uga":
                return "ug";

            case "bmu":
                return "bm";

            case "arg":
                return "ar";
            case "gbr":
                return "gb";
            default:
                System.out.println("Unknown Country: " + billing.getCountry());
                System.out.println("Unknown State: " + billing.getState());
        }
        return null;
    }

    public HashMap<String, Object> getMap(){
        HashMap<String, Object> contact = new HashMap<>();
        contact.put("street", getStreet());
        contact.put("street2", getStreet2());
        contact.put("city", getCity());
        contact.put("zip", getZip());
        contact.put("email", getEmail());
        contact.put("phone", getTel());
        String country = setOdooCountry(this);
        if(country != null){
            Integer cId = HelperFunctions.countryToId.get(country.toLowerCase());
            if(cId != null){
                contact.put("country_id", cId);
            }
            if (country.equalsIgnoreCase("us") || country.equalsIgnoreCase("ca")) {
                Integer state = HelperFunctions.stateLook.get(Pair.of(getState().toLowerCase(),cId));
                if(state != null)
                    contact.put("state_id", state);
                else
                    System.out.println("State Null " + country + " " + getState());
            }
        }

        return contact;
    }

    public String getCustomerNumber(){
        return this.customerNumber;
    }
    public void setCustomerId(String cev_no) {
        this.customerNumber = cev_no;
    }

    public void setContact(boolean b) {
        this.contact = b;
    }

    public boolean isContact(){
        return this.contact;
    }

    public Integer getOdooId(){
        return this.odooId;

    }

    public void setOdooId(Integer id){
        this.odooId = id;
    }
}
