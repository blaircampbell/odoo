package com.binnacle;

import com.opencsv.CSVWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.tinylog.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import reactor.util.concurrent.Queues;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Arrays.asList;

/**
 * Handles Sales History, Purchase History and Current Sales Orders and Purchase Orders
 */
public class Order {
    public OrderType type;
    private String invoiceAddressId;
    private String shippingAddressId;
    private String orderId;
    private String customerId;
    private List<OrderItem> orderItems = new ArrayList<>();
    private Date orderDate;
    private CustomerAddress shippingAddress;
    private CustomerAddress invoiceAddress;
    private boolean isPosOrder;
    private Date receivedDate;
    private String customerPoNumber;
    private String poNumber;
    private String termsCode;

    public BigDecimal getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(BigDecimal shippingCost) {
        this.shippingCost = shippingCost;
    }

    private BigDecimal shippingCost;

    public String getInvoiceAddressId() {
        return invoiceAddressId;
    }

    public void setInvoiceAddressId(String invoiceAddress) {
        this.invoiceAddressId = invoiceAddress;
    }

    public String getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(String invoiceAddress) {
        this.shippingAddressId = invoiceAddress;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Date getOrderDate() {
        return this.orderDate;
    }

    public void setOrderDate(String date) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
        try {
            Date dateTime = format1.parse(date);
            this.orderDate = dateTime;
        } catch (ParseException e) {
            Logger.error(e);
        }
    }

    public CustomerAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(CustomerAddress shipping) {
        this.shippingAddress = shipping;
    }

    public CustomerAddress getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(CustomerAddress invoice) {
        this.invoiceAddress = invoice;
    }

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    public void setPosOrder(boolean b) {
        this.isPosOrder = b;
    }

    public boolean isPosOrder() {
        return this.isPosOrder;
    }

    public void setReceivedDate(String date) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
        try {
            Date dateTime = format1.parse(date);
            this.receivedDate = dateTime;
        } catch (ParseException e) {
            Logger.error(e);
        }
    }

    public Date getReceivedDate() {
        return this.receivedDate;
    }

    public void setCustomerPoNumber(String po) {
        this.customerPoNumber =  po;
    }

    public String getCustomerPoNumber(){
        return this.customerPoNumber;
    }

    public void setTermsCode(String bvtermsinfocode) {
        this.termsCode = bvtermsinfocode;
    }

    public String getTermsCode(){
        return this.termsCode;
    }



    public enum OrderType {
        SALES_HISTORY,
        PURCHASE_HISTORY,
        SALES_ORDER,
        PURCHASE_ORDER
    }


    public Document getPurchaseXml(Document doc, Element root) {

        if (getCustomerId() == null || getCustomerId().isEmpty() && !isPosOrder())
            return doc;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Element record = doc.createElement("record");
        record.setAttribute("id", String.format("bin-po-%s", this.getOrderId().toLowerCase().replace(".", "").replaceAll("\\s", "-")));
        record.setAttribute("model", "purchase.order");
        root.appendChild(record);
        setRef(doc, record, "warehouse_id", "binnacle.warehouse0");

        setRef(doc, record, "partner_id", String.format("binnacle-vendors.bin-ven-%s", this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_")));
        //TODO: Need to figure out how to stitched these together

        setField(doc, record, "date_order", format1.format(getOrderDate()));
        //setField(doc,record,"picking_policy","direct");
        //setField(doc, record, "state", "done");
        setField(doc, record, "name", getOrderId());

        int i = 1;
        for (OrderItem item : getOrderItems()) {
            Element iRecord = doc.createElement("record");
            iRecord.setAttribute("id", String.format("bin-po-%s-%s", this.getOrderId(), i));
            iRecord.setAttribute("model", "purchase.order.line");
            setRef(doc, iRecord, "order_id", String.format("bin-po-%s", this.getOrderId()));
            setField(doc, iRecord, "product_uom_qty", item.getQuantity().toString());
            setField(doc, iRecord, "price_unit", item.getUnitPrice().toString());
            setRef(doc, iRecord, "product_id", String.format("binnacle-products-%s.bin-prod-%s",item.getPartCode().toLowerCase().substring(0,2), item.getPartCode().strip().toLowerCase().replace(".", "").replaceAll("\\s+", "-")));
            setRef(doc, iRecord, "product_uom", HelperFunctions.getUnitOfMeasure(item.getUnitOfMeasure()));
            setField(doc, iRecord, "date_planned", format1.format(getReceivedDate()));

            i += 1;
            root.appendChild(iRecord);
        }


        return doc;
    }

    public Queue<Map<String, Object>> getMap(int uid, Map<String, Pair<Integer, String>> prodMap, Map<String, List<CustomerAddress>> customerMap) throws IOException, XmlRpcException {
        Queue<Map<String, Object>> ret = new LinkedList<>();
        HashMap<String,Object> order = new HashMap<>();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        CustomerAddress mainContact = null;
        CustomerAddress shippingContact = null;
        CustomerAddress invoiceContact = null;
        List<HashMap> items = new ArrayList<>();

        List<CustomerAddress> addresses = customerMap.get(getCustomerId());
        if (isPosOrder()) {
            addresses = customerMap.get("pos");
        } else if(addresses == null || addresses.isEmpty()) {
            addresses = customerMap.get("unknown");
        }


        int parentOdooId = 0;
        if(customerMap != null) {
            for (CustomerAddress address : addresses) {
                if (address.isContact()) {
                    mainContact = address;
                    parentOdooId = address.getOdooId();
                } else {
                    if (getInvoiceAddress() != null && Customer.addressEquals(getInvoiceAddress(), address)) {
                        invoiceContact = address;
                    }

                    if (getShippingAddress() != null && Customer.addressEquals(getShippingAddress(), address)) {
                        shippingContact = address;
                    }
                }
            }
        }




        //order.put("warehouse_id","");
        order.put("date_order", format1.format(getOrderDate()));
        order.put("state","done");
        order.put("name",getOrderId());
        order.put("client_order_ref",getCustomerPoNumber());
        order.put("x_studio_order_book_ref",getPoNumber());
        Integer termsCode = HelperFunctions.getOdooPaymentTerms(getTermsCode());
        if(termsCode != -1){
            order.put("payment_term_id",termsCode);
        }
        if(isPosOrder()){
            order.put("partner_id", addresses.get(0).getOdooId());
            order.put("partner_invoice_id",addresses.get(0).getOdooId());
            order.put("partner_shipping_id",addresses.get(0).getOdooId());


        } else {

            if(mainContact == null) {
                System.out.println("NO CONTACT FOR " + getOrderId());
                return new LinkedList<>();
            }

            order.put("partner_id", mainContact.getOdooId());
            if(invoiceContact != null){
                if(HelperFunctions.stateToFiscalPosition.containsKey(invoiceContact.getState())){
                    order.put("fiscal_position_id", HelperFunctions.stateToFiscalPosition.get(invoiceContact.getState()));
                } else if(invoiceContact.getCountry().equals("233")){
                    order.put("fiscal_position_id", HelperFunctions.stateToFiscalPosition.get("United States"));
                } else {
                    order.put("fiscal_position_id", HelperFunctions.stateToFiscalPosition.get("United States"));
                }
                order.put("partner_invoice_id",invoiceContact.getOdooId());

            } else {
                if(HelperFunctions.stateToFiscalPosition.containsKey(mainContact.getState())){
                    order.put("fiscal_position_id", HelperFunctions.stateToFiscalPosition.get(mainContact.getState()));
                } else if(mainContact.getCountry().equals("233")){
                    order.put("fiscal_position_id", HelperFunctions.stateToFiscalPosition.get("United States"));
                } else {
                    order.put("fiscal_position_id", HelperFunctions.stateToFiscalPosition.get("United States"));
                }
                order.put("partner_invoice_id",mainContact.getOdooId());
            }

            if(shippingContact != null){
                order.put("partner_shipping_id",shippingContact.getOdooId());
            } else {
                CustomerAddress ship = getShippingAddress();
                if(ship != null && parentOdooId != 0){
                    ship = ImportMain.fixAddress(ship);
                    Map<String,Object> contact = ship.getMap();
                    contact.put("parent_id", parentOdooId);
                    contact.put("name", ship.getName());
                    contact.put("ref", ship.getCustomerNumber() + "-" + addresses.size());
                    contact.put("type", "delivery");
                    if(ship.fixed)
                        contact.put("comment", "Fixed for Odoo Import and Created For Order " + getOrderId());
                    else
                        contact.put("comment", "Created For Order " + getOrderId());
                    int localId = (Integer) HelperFunctions.getModelConfig().execute("execute_kw", asList(
                            HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                            "res.partner", "create",
                            Collections.singletonList(contact)
                    ));
                    ship.setOdooId(localId);
                    customerMap.get(getCustomerId()).add(ship);
                    order.put("partner_shipping_id",localId);
                } else {
                    order.put("partner_shipping_id",mainContact.getOdooId());
                }


            }

        }



        for (OrderItem orderItem : getOrderItems()) {
            String partCode = orderItem.getPartCode();
            HashMap<String,Object> item = new HashMap<>();
            HashMap<String,Object> noteForAbove = new HashMap<>();
            if(!prodMap.containsKey(partCode.strip())){
                item.put("display_type","line_note");
                item.put("name",cleanTextContent(orderItem.getProductDescription()));
                item.put("product_uom_qty",1);
                item.put("price_unit",0);
                item.put("product_id",false);
                item.put("product_uom",false);
                item.put("product_uom_category_id",false);
                item.put("product_type",false);
                item.put("product_template_id",false);
                item.put("x_studio_backorder_qty",0);
                item.put("qty_delivered_manual",0);
                item.put("qty_delivered_method","manual");
                item.put("is_expense",false);

            } else {
                item.put("product_uom_qty",orderItem.getQuantity().toString());
                item.put("price_unit",orderItem.getUnitPrice().toString());
                item.put("product_id",prodMap.get(partCode.strip()).getLeft());
                item.put("x_studio_backorder_qty",0);
                item.put("qty_delivered_manual",orderItem.getQuantity().toString());
                item.put("qty_delivered_method","manual");
                item.put("is_expense",false);

                /*
                        Add note after the product with information
                 */

                noteForAbove.put("display_type","line_note");
                noteForAbove.put("name",cleanTextContent(String.format("Ordered Quantity: %s, Committed  Quantity: %s, Backordered Quantity: %s",orderItem.getQuantity(),orderItem.getCommitedQuantity(),orderItem.getBackOrderQuantity())));
                noteForAbove.put("product_uom_qty",1);
                noteForAbove.put("price_unit",0);
                noteForAbove.put("product_id",false);
                noteForAbove.put("product_uom",false);
                noteForAbove.put("product_uom_category_id",false);
                noteForAbove.put("product_type",false);
                noteForAbove.put("product_template_id",false);
                noteForAbove.put("x_studio_backorder_qty",0);
                noteForAbove.put("qty_delivered_manual",0);
                noteForAbove.put("qty_delivered_method","manual");
                noteForAbove.put("is_expense",false);

            }
            items.add(item);
            if(noteForAbove.keySet().size() > 0){
                items.add(noteForAbove);
            }

        }

        if(getShippingCost() != null && getShippingCost().compareTo(BigDecimal.ZERO) > 0 )   {
            HashMap<String,Object> item = new HashMap<>();
            item.put("product_uom_qty","1");
            item.put("price_unit",getShippingCost().toString());
            item.put("product_id",prodMap.get("SHIPPING").getLeft());
            System.out.println(getOrderId() + " shipping");
            items.add(item);
        }


        List<List<Object>> l = new ArrayList<>();
        for(HashMap i : items){
            l.add(asList(0,0,i));
        }

        order.put("order_line",l);

        ret.add(order);
        return ret;
    }

    public void setPoNumber(String po){
        this.poNumber = po;
    }
    

    private String getPoNumber() {
        return this.poNumber;
    }

    private static String cleanTextContent(String text)
    {
        // strips off all non-ASCII characters
        text = text.replaceAll("[^\\x00-\\x7F]", "");

        // erases all the ASCII control characters
        text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

        // removes non-printable characters from Unicode
        text = text.replaceAll("\\p{C}", "");

        return text.trim();
    }

    public Queue<Map<String, Object>> getPurchaseMap(HashMap<String, Object> currentMeasures, Map<String, Object> binMeasures, Map<String, Pair<Integer, String>> prodMap, List<CustomerAddress> customerMap) {
        Queue<Map<String, Object>> ret = new LinkedList<>();
        HashMap<String,Object> order = new HashMap<>();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        if(customerMap == null)
            return new LinkedList<>();

        //order.put("warehouse_id","");
        order.put("date_order", format1.format(getOrderDate()));
        order.put("state","done");
        order.put("name",getOrderId());
        order.put("partner_id", customerMap.get(0).getOdooId());
        ret.add(order);
        for (OrderItem orderItem : getOrderItems()) {
            String partCode = orderItem.getPartCode();
            HashMap<String,Object> item = new HashMap<>();
            if(!prodMap.containsKey(partCode.strip())){
                item.put("display_type","line_note");
                item.put("name",cleanTextContent(orderItem.getProductDescription()));
                item.put("product_id",false);
                item.put("product_qty",0);
                item.put("price_unit",0);
                item.put("product_uom",false);
                item.put("product_uom_category_id",false);
                item.put("date_planned",false);
                item.put("product_type",false);

            } else {
                Pair<Integer,String> prod = prodMap.get(partCode.strip());
                item.put("product_qty",orderItem.getQuantity().toString());
                // item.put("product_uom_qty",orderItem.getQuantity().toString());
                item.put("price_unit",orderItem.getUnitPrice().toString());
                Integer pro = prod.getLeft();
                item.put("product_id",pro);
                item.put("product_type","product");
                item.put("name",orderItem.getProductDescription());
                item.put("display_type",false);
                HashMap map = (HashMap)currentMeasures.get(prod.getRight());
                item.put("product_uom",(Integer)map.get("id"));
                item.put("product_uom_category_id",((Object[])map.get("category_id"))[0]);
                item.put("date_planned",format1.format(getReceivedDate()));

            }
            ret.add(item);

        }

        return ret;
    }

    public Document getXml(Document doc, Element root) throws ParserConfigurationException {

        if (getCustomerId() == null || getCustomerId().isEmpty() && !isPosOrder())
            return doc;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Element record = doc.createElement("record");
        record.setAttribute("id", String.format("bin-order-%s", this.getOrderId().toLowerCase().replace(".", "").replaceAll("\\s", "-")));
        record.setAttribute("model", "sale.order");
        root.appendChild(record);
        setRef(doc, record, "warehouse_id", "binnacle.warehouse0");
        if (isPosOrder()) {
            setRef(doc, record, "partner_id", String.format("binnacle-customers-p.bin-cust-%s", "pos"));
            //TODO: Need to figure out how to stitched these together
            setRef(doc, record, "partner_invoice_id", String.format("binnacle-customers-p.bin-cust-%s", "pos"));
            setRef(doc, record, "partner_shipping_id", String.format("binnacle-customer-p.bin-cust-%s", "pos"));

        } else {
            setRef(doc, record, "partner_id", String.format("binnacle-customers-%s.bin-cust-%s",getCustomerId().length() == 0 ? getCustomerId() :  getCustomerId().toLowerCase().substring(0,2), this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_")));
            //TODO: Need to figure out how to stitched these together

            if (getInvoiceAddress() != null) {
                String invoiceId = getInvoiceAddress().getCustomerNumber();
                if (invoiceId.equalsIgnoreCase("0")) {
                    setRef(doc, record, "partner_invoice_id", String.format("binnacle-customers-%s.bin-cust-%s",getCustomerId().length() == 0 ? getCustomerId() :  getCustomerId().toLowerCase().substring(0,2),  this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_")));
                } else {
                    setRef(doc, record, "partner_invoice_id", String.format("binnacle-customers-%s.bin-cust-%s-%s",getCustomerId().length() == 0 ? getCustomerId() :  getCustomerId().toLowerCase().substring(0,2),  this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_"), invoiceId));
                }
            } else {
                setRef(doc, record, "partner_invoice_id", String.format("binnacle-customers-%s.bin-cust-%s",getCustomerId().length() == 0 ? getCustomerId() :  getCustomerId().toLowerCase().substring(0,2),  this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_")));
            }


            if (getShippingAddress() != null) {
                String shippingId = getShippingAddress().getCustomerNumber();
                if (shippingId.equalsIgnoreCase("0")) {
                    setRef(doc, record, "partner_shipping_id", String.format("binnacle-customers-%s.bin-cust-%s",getCustomerId().length() == 0 ? getCustomerId() :  getCustomerId().toLowerCase().substring(0,2),  this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_")));
                } else {
                    setRef(doc, record, "partner_shipping_id", String.format("binnacle-customers-%s.bin-cust-%s-%s",getCustomerId().length() == 0 ? getCustomerId() :  getCustomerId().toLowerCase().substring(0,2),  this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_"), shippingId));
                }
            } else {
                setRef(doc, record, "partner_shipping_id", String.format("binnacle-customers-%s.bin-cust-%s",getCustomerId().length() == 0 ? getCustomerId() :  getCustomerId().toLowerCase().substring(0,2),  this.getCustomerId().toLowerCase().replace(".", "").replaceAll("\\s", "_")));
            }
        }

        setField(doc, record, "date_order", format1.format(getOrderDate()));
        //setField(doc,record,"picking_policy","direct");
        setField(doc, record, "state", "done");
        setField(doc, record, "name", getOrderId());

        int i = 1;
        for (OrderItem item : getOrderItems()) {
            Element iRecord = doc.createElement("record");
            iRecord.setAttribute("id", String.format("bin-order-%s-%s", this.getOrderId(), i));
            iRecord.setAttribute("model", "sale.order.line");
            setRef(doc, iRecord, "order_id", String.format("bin-order-%s", this.getOrderId()));
            setField(doc, iRecord, "product_uom_qty", item.getQuantity().toString());
            setField(doc, iRecord, "price_unit", item.getUnitPrice().toString());
            setRef(doc, iRecord, "product_id", String.format("binnacle-products-%s.bin-prod-%s",item.getPartCode().toLowerCase().substring(0,2), item.getPartCode().strip().toLowerCase().replace(".", "").replaceAll("\\s+", "-")));

            /*
            <field name="product_uom_qty">3</field>
            <field name="product_uom" ref="uom.product_uom_unit"/>
            <field name="price_unit">2950.00</field>
             */
            i += 1;
            root.appendChild(iRecord);
        }


        return doc;
    }

    private void setField(Document doc, Element record, String n, String value) {
        if (value == null)
            return;
        Element name = doc.createElement("field");
        name.setAttribute("name", n);
        name.setTextContent(value.strip());
        record.appendChild(name);
    }

    private void setRef(Document doc, Element record, String n, String value) {
        if (value == null)
            return;
        Element name = doc.createElement("field");
        name.setAttribute("name", n);
        name.setAttribute("ref", value.strip());
        record.appendChild(name);
    }
}
