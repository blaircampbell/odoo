package com.binnacle;

import org.apache.xmlrpc.XmlRpcException;
import org.tinylog.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.ParserConfigurationException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Arrays.asList;

public class Product {

    public static ExecutorService exService = Executors.newCachedThreadPool();
    private Date date;
    private String warehouse;
    private String code;
    private String description;
    private BigDecimal onHand;
    private String unitOfMeasure;
    private String barcode;
    private BigDecimal listPrice;
    private String price;
    private BigDecimal standardPrice;
    private String priceUnitOfMeasure;

    public static boolean doesBarcodeExist(int uid, String barcode) throws MalformedURLException, XmlRpcException {
        if (barcode.isEmpty())
            return false;
        final List<Object> ids = asList((Object[]) HelperFunctions.getModelConfig().execute(
                "execute_kw", asList(
                        HelperFunctions.SERVER, uid, HelperFunctions.PASSWORD,
                        "product.product", "search",
                        Collections.singletonList(Collections.singletonList(
                                asList("barcode", "=", barcode)
                        )),
                        new HashMap<>() {{
                            put("limit", 1);
                        }})));

        return ids.size() > 0;
    }



    public Date getDate() {
        return date;
    }

    public void setDate(String date) {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
        try {
            Date dateTime = format1.parse(date);
            this.date = dateTime;
        } catch (ParseException e) {
            Logger.error(e);
        }
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String whse) {
        this.warehouse = whse;

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getOnHand() {
        return onHand;
    }

    public void setOnHand(BigDecimal onHand) {
        this.onHand = onHand;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public BigDecimal getStandardPrice() {
        return standardPrice;
    }

    public void setStandardPrice(BigDecimal standardPrice) {
        this.standardPrice = standardPrice;
    }

    public String getPriceUnitOfMeasure() {
        return priceUnitOfMeasure;
    }

    public void setPriceUnitOfMeasure(String priceUnitOfMeasure) {
        this.priceUnitOfMeasure = priceUnitOfMeasure;
    }


    /*
    <record id="product_delivery_02" model="product.product">
            <field name="name">Office Lamp</field>
            <field name="categ_id" ref="product_category_5"/>
            <field name="standard_price">35.0</field>
            <field name="list_price">40.0</field>
            <field name="type">consu</field>
            <field name="weight">0.01</field>
            <field name="uom_id" ref="uom.product_uom_unit"/>
            <field name="uom_po_id" ref="uom.product_uom_unit"/>
            <field name="default_code">FURN_8888</field>
            <field name="image_1920" type="base64" file="product/static/img/product_lamp.png"/>
        </record>

     */
    public Document getXml(Document doc, Element root) throws ParserConfigurationException{

        Element record = doc.createElement("record");
        record.setAttribute("id", String.format("bin-prod-%s",this.getCode().toLowerCase().replace(".","").replaceAll("\\s","-")));
        record.setAttribute("model", "product.product");

        setField(doc,record,"name",getDescription());
        setField(doc,record,"default_code",getCode());

        if(getBarcode() != null && !getBarcode().isEmpty())
            setField(doc,record,"barcode",getBarcode());

        setField(doc,record,"standard_price",getStandardPrice().toString());
        setField(doc,record,"list_price",getListPrice().toString());
        setField(doc,record,"type","product");


        setRef(doc,record,"uom_id",HelperFunctions.getUnitOfMeasure(getUnitOfMeasure()));
        setRef(doc,record,"uom_po_id",HelperFunctions.getUnitOfMeasure(getPriceUnitOfMeasure()));

        root.appendChild(record);

        return doc;
    }

    private void setField(Document doc, Element record, String n, String value){
        if(value == null)
            return;
        Element name = doc.createElement("field");
        name.setAttribute("name",n);
        name.setTextContent(value.strip());
        record.appendChild(name);
    }

    private void setRef(Document doc, Element record, String n, String value){
        if(value == null)
            return;
        Element name = doc.createElement("field");
        name.setAttribute("name",n);
        name.setAttribute("ref",value.strip());
        record.appendChild(name);
    }

    public HashMap<String, Object> getMap() {
        HashMap<String,Object> m = new HashMap<>();
        m.put("name",getDescription().strip());
        m.put("default_code",getCode().strip());

        if(getBarcode() != null && !getBarcode().isEmpty())
            m.put("barcode", getBarcode().strip());

        m.put("standard_price",getStandardPrice().toString());
        m.put("list_price",getListPrice().toString());
        m.put("type","product");

        m.put("uom_id",getUnitOfMeasure().strip());
        m.put("uom_po_id",getPriceUnitOfMeasure().strip());

        return m;
    }
}
