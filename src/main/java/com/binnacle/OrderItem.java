package com.binnacle;

import java.math.BigDecimal;

public class OrderItem {

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getCommitedQuantity() {
        return commitedQuantity;
    }

    public void setCommitedQuantity(BigDecimal commitedQuantity) {
        this.commitedQuantity = commitedQuantity;
    }

    public BigDecimal getBackOrderQuantity() {
        return backOrderQuantity;
    }

    public void setBackOrderQuantity(BigDecimal backOrderQuantity) {
        this.backOrderQuantity = backOrderQuantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getPartCode() {
        return partCode;
    }

    public void setPartCode(String partCode) {
        this.partCode = partCode;
    }

    private String partCode;
    private String productDescription;
    private String productCode;
    private String unitOfMeasure;
    private BigDecimal quantity;
    private BigDecimal commitedQuantity;
    private BigDecimal backOrderQuantity;
    private BigDecimal unitPrice;
    private String warehouse;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    private String orderId;
}
